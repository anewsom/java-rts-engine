package JavaGameP;

public class ClassLoad{
    
    public static GameObject load_string(String name,Gamemap game,String owner){
        Player temppl=null;
        int ownerint=Integer.parseInt(owner);
         if(ownerint==0)
            temppl=game.players[0];
        else if(ownerint==1)
            temppl=game.players[1];
        else if(ownerint==2)
            temppl=game.players[2];
        return load_string(name,game,temppl);
    }
    public static GameObject load_string(String name,Gamemap game,Player owner){
        GameObject obj=null;
        
        if(name.equals("Marine"))//works
            obj=new Marine();
        
        else if(name.equals("Terrain"))//Gaia
            obj=new TerrainObject();
        else if(name.equals("Mineral"))
            obj=new Mineral();
        
        else if(name.equals("Base"))//Buildings
            obj=new Base();
        else if(name.equals("Barracks"))
            obj=new Barracks();
        else if(name.equals("Shop"))
            obj=new Shop();
        else if(name.equals("SpecialOps"))
            obj=new SpecialOps();
            
        else if(name.equals("Drone"))//Units
            obj=new Drone();
        else if(name.equals("RocketTrooper"))
        	obj=new RocketTrooper();
        else if(name.equals("Jedi"))
            obj=new Jedi();
        else if(name.equals("Paladin"))
            obj=new Paladin();
        else if(name.equals("SCV"))
            obj=new SCV();
        else if(name.equals("Sniper"))
            obj=new Sniper();
                        
        if(obj!=null)
        owner.setupUnit(obj);
        
        //System.out.println("= "+obj.getbuildspeed());
        return obj;
    }
    public static Weapon load_weapon_string(String name,FightingUnit newholder){
        Weapon wep=null;
        if(name.equals("LightMachineGun"))//works
            wep=new LightMachineGun(newholder);
        else if(name.equals("RocketLauncher"))
            wep=new RocketLauncher(newholder);
        else if(name.equals("FusionCutter"))
            wep=new FusionCutter((Harvester)newholder);
        else if(name.equals("LightSaber"))
            wep=new LightSaber(newholder);
        else if(name.equals("SniperRifle"))
            wep=new SniperRifle(newholder);
        else if(name.equals("TankShell"))
            wep=new TankShell((Tank)newholder);
        return wep;
    }
}
