package JavaGameP;
import java.awt.*;

public abstract class Harvester extends FightingUnit {
    protected boolean carryingMineralB=false,first=true;
    protected transient Image MineralIm;
    public Base returnTarget;
      public Harvester(){
        super();
        clas=3;
            }
    
    public Harvester(Gamemap newgame, Zone z, int newhitpoints, int newmovementrate, String newname, Player newowner, int newbuildspeed, Resource newcost,int newnum_attack_animation_frames, HarvestingWeapon newweapon) {
        super(newgame, z, newhitpoints, newmovementrate, newname, newowner, 3, newbuildspeed, newcost,newnum_attack_animation_frames, newweapon);
        MineralIm=(Image) game.getImages().get(name+"Mineral");
    }

    public String[] save(){
        String[]savestring=super.save();
        //line 14 = carryingMineralB
        savestring[14]=Boolean.toString(carryingMineralB);
        //line 15 = return BaseID
        if(returnTarget!=null)
        savestring[15]=Integer.toString(returnTarget.getID());
        else
         savestring[15]="null";   
        return savestring;
    }
    
    public void setTarget(GameObject newtarget) {
        if (newtarget.isMinable()) //need to check if target is instace of mineral
        {
            target = newtarget;
            moving = false;
        }
        if(newtarget.isBase()&&carryingMineralB)
            returnTarget=(Base)newtarget;
    }
    public void setmoveTarget(Point np) {
        // System.out.println ("In setmoveTarget"); //Debug
        moving = true;
        target = null;
        returnTarget=null;
        moveto = np;
        //super.setmoveTarget(np);
    }
    public void setcarryingMineralB(boolean newb){
        carryingMineralB=newb;
           }
    public boolean getcarryingMineralB(){
        return carryingMineralB;
    }
    public void act(){
         if(first){
         MineralIm=(Image) game.getImages().get(name+"Mineral");
         first=false;
        }
        if(!carryingMineralB){
            if (target != null && myweapon.inrange(target))
                moving = false;
            if (!moving && target != null && target.exists())
                myweapon.attack(target);
            else if (!moving && target != null && !target.exists()) {
                target=null;
                //cancel_moving();
                setImageback();
            }
            else {
                move();
            }
        }
        else{
            //System.out.println("In else1()");//Debug
            if (returnTarget != null && myweapon.inrange(returnTarget))
                moving = false;
            if (!moving && returnTarget != null && returnTarget.exists())
                setReturnMineral(false);
            else if (!moving && returnTarget != null && !returnTarget.exists()) {
                returnTarget=null;
                //cancel_moving();
                //System.out.println("In else2()");//Debug
            }
            else if(returnTarget != null){
                //System.out.println("In else3()");//Debug
                returnTargetMove();
                //System.out.println("In else3()");//Debug
            }
            else
                move();
        }
    }
    public void redoReturnTarget(){
        if(carryingMineralB){
            returnTarget = closest_Base();
            moving=true;
            moveto = new Point(returnTarget.getZone().x,returnTarget.getZone().y);
        }
    }
    protected boolean returnTargetMove(){
        // if(returnTarget==null)
        //    return false;
        Point tmppt= new Point(returnTarget.getZone().x,returnTarget.getZone().y);
        if (Math.abs(tmppt.x - x) < movementrate && Math.abs(tmppt.y - y) < movementrate)
            return false;
        boolean moved = false;
        if (tmppt.x < x) {
            moveLeft();
            moved = true;
        }
        else if (tmppt.x > x) {
            moveRight();
            moved = true;
        }
        if (tmppt.y > y) {
            moveDown();
            moved = true;
        }
        else if (tmppt.y < y) {
            moveUp();
            moved = true;
        }
        if (moved == false)
            cancel_moving();
        return moved;
    }
    protected Base closest_Base(){
        //System.out.println("In closest_Base()");//Debug
        GameObject[] re = game.getobjin(new Zone(x - 500, y - 500, 1000, 1000));
        int distance = 10000, mostsave = -1, tx, ty;
        for (int d = 0 ; d < re.length ; d++) {
            if (isAlly(re [d])&&re[d].isBase()) {
                tx = re [d].getCenter().x;
                ty = re [d].getCenter().y;
                if (distance > (int) Math.sqrt(tx * tx + ty * ty)) {
                    distance = (int) Math.sqrt(tx * tx + ty * ty);
                    mostsave = d;
                }
            }
        }
        if (mostsave == -1)
            return null;
        //setTarget(re [mostsave]);
        return (Base)re [mostsave];
    }
    public void setReturnMineral(boolean newcarryingMineralB){
        carryingMineralB=newcarryingMineralB;
        setImageMin(carryingMineralB);
        if(carryingMineralB){
            returnTarget = closest_Base();
            moving = true;
        }
        else{
            owner.addminerals(myweapon.getattackdamage()*((HarvestingWeapon)myweapon).get_max_carry_counter());
        }
    }
    protected void setImageMin(boolean carryingmin){
        if(carryingmin)
            im=MineralIm;
        else
            setImageback();
    }
}
