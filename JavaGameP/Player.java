package JavaGameP;
import java.awt.*;
import java.io.Serializable;

/**
 *	The Player- human and otherwise
 */
public class Player implements Serializable {
    private int num, minerals = 0;
    private int armorlevel,JediLevels=1;
    private transient GameDisplay dp;
    private Color col;
    
    public Player(int newnum){
        num = newnum;
    }
    
    public Player(int newnum,GameDisplay newdp) {
        dp=newdp;
        num = newnum;
    }
    
    public void setDis(GameDisplay newdp){
         dp=newdp;
    }
    
    public void setColor(Color newcol){
        col=newcol;
    }
    public Color getColor(){
        return col;
    }
    public int getnum() {
        return num;
    }
    public void setJediLevel(int newJediLevel ){
        JediLevels =newJediLevel;
    }
    
    public int getJediLevel() {
        return JediLevels;
    }
    
    public int getArmorlevel(){
        return armorlevel;
    }
    
    public void addminerals(int add) {
        minerals += add;
    }
    
    public void setminerals(int set) {
        minerals = set;
    }
    
    
    public int getminerals() {
        return minerals;
    }
    
    
    public void takeminerals(int sub) {
        minerals -= sub;
    }
    
    public void setupUnit(GameObject obj){
        obj.setgame(dp.g);
        obj.changeowner(this);
        obj.setcost((Resource)dp.Costs.get(obj.getName()));
        
        if(obj.getJedi())
            ((Jedi)obj).set_Jedi_level(JediLevels);
    }
}
