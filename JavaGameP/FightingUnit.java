package JavaGameP;

import java.util.*;
import java.awt.*;

public abstract class FightingUnit extends Unit {
    protected Weapon myweapon;
    protected GameObject target;
    protected boolean moving = false, attack_move_b = false; // default stance for attack move
    protected int counter2 = 0,attack_animation_counter=-1,num_attack_animation_frames;
    protected transient Image [] attackImages= new Image [10];
    
    public FightingUnit(){
        super();
        fight=true;
        
    }
    
    public FightingUnit(Gamemap newgame, Zone z, int newhitpoints, int newmovementrate, String newname, Player newowner, int newclas, int newbuildspeed, Resource newcost,int newnum_attack_animation_frames, Weapon newweapon) {
        super(newgame, z, newhitpoints, newmovementrate, newname, newowner, newclas, newbuildspeed, newcost);
        myweapon = newweapon;
        num_attack_animation_frames=newnum_attack_animation_frames;
        attackImages[0]=(Image) game.getImages().get(name + " Attack");
        fight = true;
    }
    
    
    public void setgame(Gamemap newgame){
        super.setgame(newgame);
        attackImages = new Image [10];
        attackImages[0]=(Image) game.getImages().get(name + " Attack");
        myweapon.setholder(this);
        myweapon.setAttackImage(attackImages[0]);
    }
    
    public void load(String [] loadstring,GameDisplay dis)throws NumberFormatException{
        super.load(loadstring,dis);
        counter2=Integer.parseInt(loadstring[9]);

        myweapon= ClassLoad.load_weapon_string(loadstring[10],this);
         moving = Boolean.getBoolean(loadstring[11]);
        attack_move_b = Boolean.getBoolean(loadstring[12]);
//loadstring[13]is target ID
    }
    
    public String[] save(){
        String[]savestring=super.save();
        //line 9 = counter2
        savestring[9]=Integer.toString(counter2);
        //line 10 = weapon name
        savestring[10]=myweapon.getName();
        //line 11 = moving
        savestring[11]=Boolean.toString(moving);
        //line 12 = attack_move_b
        savestring[12]=Boolean.toString(attack_move_b);
        //line 13 = targetID
        if(target!=null)
        savestring[13]=Integer.toString(target.getID());
        else
            savestring[13]="null";
        return savestring;
    }
    
    public void setattackImage(){
        attack_animation_counter=0;
        tempsetImage(attackImages[attack_animation_counter]);
    }
    public void set_attack_move(boolean new_attack_move_b) {
        attack_move_b = new_attack_move_b;
    }
    
    public boolean isTargetNull(){
    	if(target==null)
	    	return true;
    	return false;
    }
    
    public Weapon getweapon() {
        return myweapon;
    }
    
    
    public void setweapon(Weapon neww) {
        myweapon = neww;
    }
    
    
    public void setTarget(GameObject newtarget) {
        if (!isAlly(newtarget)) {
            target = newtarget;
            moving = false;
        }
        else
            super.setTarget(newtarget);
    }
    
    
    public void setmoveTarget(Point np) {
        // System.out.println ("In setmoveTarget"); //Debug
        moving = true;
        target = null;
        setImageback();
        super.setmoveTarget(np);
    }
    
    
    public void setattackmoveTarget(Point np) {
        // System.out.println ("In setattackmoveTarget"); //Debug
        moving = true;
        super.setmoveTarget(np);
    }
    
    
    public void act() {
        if (target != null && myweapon.inrange(target))
            moving = false;
        if (!moving && target != null && target.exists())
            myweapon.attack(target);
        else if (!moving && target != null && !target.exists()) {
            target=null;
            cancel_moving();
            setImageback();
        }
        else {
            counter++;
            boolean moveB=move();
            // if(moveB){
            // mydir = direct (x, y, moveto); // *****working, need to fix later*****
            //System.out.println(mydir.getDirection());//Debug)
            //  }
            if (!moveB && counter == 10) //moves
                checkforenemy();
            else if (attack_move_b && target == null && counter == 10)
                checkforenemy();
            if (counter == 10)
                counter = 0;
        }
        if(attack_animation_counter!=-1){
            attack_animation_counter++;
            if(attack_animation_counter==num_attack_animation_frames)
                num_attack_animation_frames=-1;
        }
    }
    
    
    
    protected boolean checkforenemy() {
        GameObject[] re = game.getobjin(new Zone(x - myweapon.getrange(), y - myweapon.getrange(), x + myweapon.getrange(), y + myweapon.getrange()));
        int distance = 10000, mostsave = -1, tx, ty;
        for (int d = 0 ; d < re.length ; d++) {
            if (!isAlly(re [d])&&re[d].getfight()) {
                tx = re [d].getCenter().x;
                ty = re [d].getCenter().y;
                if (distance > (int) Math.sqrt(tx * tx + ty * ty)) {
                    distance = (int) Math.sqrt(tx * tx + ty * ty);
                    mostsave = d;
                }
            }
        }
        if (mostsave == -1) {
            for (int d = 0 ; d < re.length ; d++) {
                if (!isAlly(re [d])) {
                    tx = re [d].getCenter().x;
                    ty = re [d].getCenter().y;
                    if (distance > (int) Math.sqrt(tx * tx + ty * ty)) {
                        distance = (int) Math.sqrt(tx * tx + ty * ty);
                        mostsave = d;
                    }
                }
            }
        }
        if (mostsave == -1) {
        return false;
        }
        setTarget(re [mostsave]);
        return true;
    }
}
