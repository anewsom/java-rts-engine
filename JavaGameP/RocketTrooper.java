package JavaGameP;

import java.awt.*;
import java.util.*;
public class RocketTrooper extends Infantry {
    public RocketTrooper(){
        super();
        width=30;
        height=30;
        name="RocketTrooper";
        buildspeed=40;
        myweapon = new RocketLauncher(this);
        attack_animation_counter=1;        
    }

    
    public RocketTrooper(Gamemap newgame, Zone z, Player newowner) {
        super(newgame, new Zone(z.x,z.y,30,30), "RocketTrooper", newowner, 35, new Resource(60, 0),1, null);
        myweapon = new RocketLauncher(this);
    }
}
