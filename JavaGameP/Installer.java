package JavaGameP;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.imageio.*;
import java.nio.channels.*;

/**
 *	Installer Class             ***Not Yet Functioning***
 *	Meant to be a stand alone installer- yet still depends on ErrorGen
 */
public class Installer extends Frame implements WindowListener {
    
    private String abs_dir_path="C:\\Program Files\\Window Wars", original_path=":\\Install\\JavaGame";
    private int resX, resY,cX,cY,numfiles=5;
    private TextField path = new TextField(abs_dir_path),driveLetter = new TextField("D");
    public Canvas screen=new Canvas();
    private Image install, browse;
    private boolean choseDirB=false,installB=false,HDWerror=false,Writing=false,Writingerror=false,donein=false,nocd=false;
    
    public Installer(){
        super("Window Wars Installer");
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        cX=ge.getCenterPoint().x;
        cY=ge.getCenterPoint().y;
        resX=ge.getMaximumWindowBounds().x;
        resY=ge.getMaximumWindowBounds().y;
        
        setLayout(new BorderLayout());
        setLocation(cX-400,cY-300);
        setSize(800,625);
        addWindowListener(this);
        setResizable(false);
        
        browse= getToolkit().getImage("Browse.jpg");
        prepareImage(browse, screen);
        
        install= getToolkit().getImage("Installi.jpg");
        prepareImage(install, screen);
        
        add("South",path);
        add("Center",screen);
        add("North",driveLetter);
        
        // try{Thread.sleep(2000);}
        //catch(InterruptedException ee){}
        setVisible(true);
        run();
    }
    private void run(){
        
        while(true){
            drawWords();
            if(choseDirB)
                choseDir();
            else if(installB)
                try{
                    install_run();
                }catch(IOException IOE){new ErrorGen(this, "IOE",IOE);}
            
            
            try{Thread.sleep(500);}
            catch(InterruptedException ee){}
        }
    }
    private boolean create_dir(){
        File parent_dir = new File(path.getText());
        return parent_dir.mkdirs();
    }
    private void install_run()throws IOException{
        Writing=true;
        path.setEditable(false);
        
        HDWerror=false;
        if(!create_dir()){
            HDWerror=true;
            Writing=false;
            return;
        }
        driveLetter.setEditable(false);
        File instJavaGame=new File(driveLetter.getText()+original_path);
        if(!instJavaGame.canRead()){
            nocd=true;
            driveLetter.setEditable(true);
            return;
        }
        File outJavaGame=new File(path.getText());
        copyFilesInDir(instJavaGame,outJavaGame);
        if(nocd){
            driveLetter.setEditable(true);
            return;
        }
        if(Writingerror){
            new File(path.getText()).delete();
            path.setEditable(true);
            return;
        }
        //System.out.println("Error Writing to Hard Drive \"C\" on C:\\Program Files\\Window Wars");
        Writingerror=false;
        installB=false;
        Writing=false;
        donein=true;
        nocd=false;
    }
    private void copyFilesInDir(File parentDir,File destParentDir)throws IOException{
        if(!parentDir.isDirectory())
            Writingerror=false;
        File [] filesInDir=parentDir.listFiles();
        for(int d=0;d<filesInDir.length;d++){
            if(filesInDir[d].isDirectory()){
                File desDir=new File(destParentDir.getAbsolutePath()+"\\"+filesInDir[d].getName());
                desDir.mkdir();
                copyFilesInDir(filesInDir[d],desDir);
                if(nocd)
                    return;
            }
            
            FileInputStream filein;
            try{
                filein=new FileInputStream(filesInDir[d]);
            }
            catch(FileNotFoundException FNF){
                nocd=true;
                return;
            }
            FileChannel filechin=filein.getChannel();
            int filesize=(int)filechin.size();
            byte [] fileinfo=new byte [filesize];
            FileOutputStream fileout;
            File outFile= new File(destParentDir.getAbsolutePath()+"\\"+filesInDir[d].getName());
            try{
                fileout=new FileOutputStream(outFile);
            }catch(FileNotFoundException FNFer){
                Writingerror=true;
                System.out.println("here");
                return;
            }
            FileChannel filechout = fileout.getChannel();
            filechin.transferTo(0,filechin.size(),filechout);
            filechin.close();
            filechout.close();
            fileout.close();
            filein.close();
            
        }
    }
    private void drawWords(){
        Graphics g=screen.getGraphics();
        g.setColor(Color.white);
        g.fillRect(0, 0, 600, 400);
        g.drawImage(browse,735,515,60,30,null);
        g.drawImage(install,0,515,100,30,null);
        g.setColor(Color.black);
        g.drawString("CD Drive Letter (ex. D)...",50,30);
        if(nocd)
            g.drawString("No CD dectected in Drive "+driveLetter.getText()+"...",10,90);
        if(HDWerror)
            g.drawString("Error Writing to " + path.getText(),10,70);
        if(Writing)
            g.drawString("Installing...",10,10);
        if(donein&&Writingerror)
            g.drawString("Instalation Failure... Your Computer was not changed",10,50);
        else if(donein&&!Writingerror)
            g.drawString("Instalation Succesful...",10,50);
    }
    
    public static void IWishmain(String[] args) {//changed
        new Installer();
    }
    private void choseDir(){
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Install To");
        chooser.setApproveButtonText("Install To");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setCurrentDirectory(new File("C:\\Program Files\\"));
        int returnVal = chooser.showOpenDialog(new Frame());
        
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            path.setText( chooser.getSelectedFile().getAbsolutePath());
        }
        choseDirB=false;
    }
    public boolean mouseDown(Event Mousem, int x, int y){
        //System.out.println(x+"  "+y);//Debug
        if(x>740&&x<800&&y>545&&y<600)
            choseDirB=true;
        else if(x<100&&y>545&&y<600)
            installB=true;
        
        return true;
    }
    //---------------------------------------------------------------------------------------
    // The following seven methods are defined in the
    // WindowListener interface.
    public void windowActivated(WindowEvent ee) {
    }
    
    
    public void windowClosing(WindowEvent ee) {
        System.gc();
        System.exit(0); // Quit the program.
    }
    
    
    public void windowClosed(WindowEvent ee) {
    } // windowClosed method
    
    
    public void windowDeactivated(WindowEvent ee) {
    } // windowDeactivated method
    
    
    public void windowDeiconified(WindowEvent ee) {
    } // windowIconified method
    
    
    public void windowIconified(WindowEvent ee) {
    } // windowIconified method
    
    
    public void windowOpened(WindowEvent ee) {
    } //------------------------------------------------------------------------------------
    
}
