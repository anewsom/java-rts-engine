package JavaGameP;

public class Mineral extends GameObject
{
    public Mineral(){
        super();
        minable = true;
        hitpoints=1500;
        width=30;
        height=30;
        fullhitpoints=1500;
        name="Mineral";
        cost=new Resource (0, 0);
        buildspeed=1;
        clas=3;
    }
    public Mineral (Gamemap newgame, Zone z)
    {
	super (newgame, new Zone (z.x,z.y,30,30), 1500, "Mineral", newgame.players[0],3, new Resource (0, 0), 1);
	minable = true;
    }
}
