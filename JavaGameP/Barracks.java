package JavaGameP;

import java.awt.Image;
import java.util.ArrayList;
public class Barracks extends ProductionBuilding {
    public Barracks(){
        width=90;
        height=90;
        fullhitpoints=1000;
        hitpoints=1000;
        name="Barracks";
        cost=new Resource(150, 0);
        buildspeed=150;
        productionitems=new ArrayList();
        productionitems.add("Marine");
        productionitems.add("RocketTrooper");
    }
    public Barracks(Gamemap newgame, Zone z, Player newowner) {
        super(newgame, new Zone(z.x,z.y,90,90), 100, "Barracks", newowner, new ArrayList(), new Resource(150, 0), 150);
        productionitems.add("Marine");
        productionitems.add("RocketTrooper");
    }
}
