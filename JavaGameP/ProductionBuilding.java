package JavaGameP;

import java.util.ArrayList;
import java.util.LinkedList;
public abstract class ProductionBuilding extends Building {
    public ArrayList productionitems;
    public LinkedList que = new LinkedList();
    private int countdownproduction;
    boolean first = true;
    
    public ProductionBuilding(){
        super();
        factory = true;
    }
    public ProductionBuilding(Gamemap newgame, Zone z, int newhitpoints, String newname, Player newowner, ArrayList newproductionitems, Resource newcost, int newbuildspeed) {
        super(newgame, z, newhitpoints, newname, newowner, newcost, newbuildspeed);
        productionitems = newproductionitems;
        factory = true;
    }    
    
    public void addProduction(String newUnitString) {
        //System.out.println ("Entering addProduction() newUnit.getcost ().res1==" + newUnit.getcost ().res1 + " owner.getminerals ()= " + owner.getminerals ()); //Debug
        
        if(newUnitString.equals("Upgrade Jedi Level + 1"))//for upgrades
        {
        	 if (owner.getminerals() >= game.dis.Costs.get(newUnitString).res1)
        	 {
        	 owner.setJediLevel(owner.getJediLevel()+1);
        	 owner.takeminerals(game.dis.Costs.get(newUnitString).res1);
        	 }
        }
        else
        {
        Unit newUnit=(Unit)ClassLoad.load_string(newUnitString,game,owner);
        if (owner.getminerals() < newUnit.getcost().res1||que.size()==6) //need check for resource 2**
            return;
        owner.takeminerals(newUnit.getcost().res1);
        que.addLast(newUnit);
        if (first) {
            countdownproduction = ((Unit) que.getFirst()).getbuildspeed();
            //System.out.println(countdownproduction);//Debug
            first = false;
        }
        }
    }
    public void removeProduction(int quenum){
        owner.addminerals(((GameObject)que.remove(quenum)).getcost().res1);
        if(que.size()==0)
            first = true;
    }
    
    public void act() {
    	//if(game.getDebug()) System.out.println("PB act-countdownproduction="+countdownproduction+"first="+first);
        if (que.size() != 0)
            production();
        super.act();
    }
    
    
    protected void production() {
        countdownproduction--;
        if (countdownproduction > 0)
            return;
        Unit tempu= (Unit)que.removeFirst();
        if(!create(tempu))
            owner.addminerals(tempu.getcost().res1);
        // System.out.println (que.size ()); //Debug
        if (que.size() != 0) {
            countdownproduction = ((Unit)que.getFirst()).getbuildspeed();
            //System.out.println (" production " + ((Unit) que.getLast ()).getbuildspeed ()); //Debug
            //System.out.println ("changeing countdownproduction to = " + countdownproduction); //Debug
        }
        else
            first = true;
    }
    
    
    public int getcurrentcount() {
        return countdownproduction;
    }
    
    
    protected boolean create(Unit newobj) {
        //System.out.println ("create " + newobj.fight);//Debug
        Zone newzone;
        if (newobj.getfight())
            ((FightingUnit) newobj).getweapon().setholder((FightingUnit) newobj);
        newzone = new Zone(x + width + 1, y + height + 1, newobj.width, newobj.height);
        if (game.isEmpty(newzone)) {
            newobj.setlocation(x + width + 1, y + height + 1);
            newobj.cancel_moving();
            newobj.add(newzone);
            return true;
        }
        newzone = new Zone(x, y + height + 1, newobj.width, newobj.height);
        if (game.isEmpty(newzone)) {
            newobj.setlocation(x, y + height + 1);
            newobj.cancel_moving();
            newobj.add(newzone);
            return true;
        }
        newzone = new Zone(x + width + 1, y, newobj.width, newobj.height);
        if (game.isEmpty(newzone)) {
            newobj.setlocation(x + width + 1, y);
            newobj.cancel_moving();
            newobj.add(newzone);
            return true;
        }
        newzone = new Zone(x - newobj.width - 1, y - newobj.height - 1, newobj.width, newobj.height);
        if (game.isEmpty(newzone)) {
            newobj.setlocation(x - newobj.width - 1, y - newobj.height - 1);
            newobj.cancel_moving();
            newobj.add(newzone);
            return true;
        }
        return false;
    }
    
    public ArrayList getProductionItems() {
        return productionitems;
    }
    
    public LinkedList getque() {
        return que;
    }
}


