package JavaGameP;

import java.awt.Image;
import java.util.ArrayList;
public class Base extends ProductionBuilding {
    public Base(){
        super();
        width=100;
        height=100;
        fullhitpoints=1200;
        hitpoints=1200;
        name="Base";
         cost=new Resource(400, 0);
        buildspeed=350;
        productionitems=new ArrayList();
        productionitems.add("SCV");
        productionitems.add("Drone");
        isbase=true;
    }
    public Base(Gamemap newgame, Zone z, Player newowner) {
        super(newgame, new Zone(z.x,z.y,100,100), 1200, "Base", newowner, new ArrayList(), new Resource(400, 0), 350);
        productionitems.add("SCV");
        productionitems.add("Drone");
        isbase=true;
    }
}
