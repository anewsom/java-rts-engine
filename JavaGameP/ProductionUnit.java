package JavaGameP;

import java.util.ArrayList;
import java.util.LinkedList;
import java.awt.*;
public class ProductionUnit extends Unit {
    protected ArrayList productionitems;
    protected LinkedList que = new LinkedList();
    protected int countdownproduction, uplink_building_number=-1;
    protected boolean first = true;
    protected IZone target;
    //protected boolean builder=true;
    
    public ProductionUnit(){
        super();
        factory=true;
    }
    public ProductionUnit(Gamemap newgame, Zone z, int newhitpoints, int newmovementrate, String newname, Player newowner, int newclas, int newbuildspeed, Resource newcost,ArrayList newproductionitems) {
        super(newgame, z, newhitpoints, newmovementrate, newname, newowner, newclas, newbuildspeed,  newcost);
        productionitems = newproductionitems;
        factory=true;
    }
    
    /*public void load(String [] loadstring,GameDisplay dis)throws NumberFormatException{
        super.load(loadstring,dis);
        if(!loadstring[9].equals("null")){
            que.addLast(ClassLoad.load_string(loadstring[9],game,owner));
        }
        first=Boolean.getBoolean(loadstring[10]);
        countdownproduction=Integer.parseInt(loadstring[11]);
        if(!loadstring[12].equals("null")){
            target=new IZone(Integer.parseInt(loadstring[12]),Integer.parseInt(loadstring[13]),Integer.parseInt(loadstring[14]),Integer.parseInt(loadstring[15]));
        }
    }*/
    public void setuplink_building_number(int newer)
    {
    	uplink_building_number=newer;
    }
    public int getuplink_building_number()
    {
    	return uplink_building_number;
    }
    
    /*public String[]save(){
        String[]savestring=super.save();
        //line 9 = que unit name
        if(que.size()!=0)
            savestring[9]=((GameObject)que.getFirst()).getName();
        else
            savestring[9]="null";
        
        //line 10 = first
        savestring[10]=Boolean.toString(first);
        //line 11 = countdownproduction
        savestring[11]=Integer.toString(countdownproduction);
        //line 12-13-14-15 target zone
        if(target!=null){
            savestring[12]=Integer.toString(target.x);
            savestring[13]=Integer.toString(target.y);
            savestring[14]=Integer.toString(target.width);
            savestring[15]=Integer.toString(target.height);
        }
        else
            savestring[12]="null";
        
        return savestring;
    }*/
    
    public void addProduction(String newBuildingString,Zone newItarget) {
    	IZone newtarget=newItarget.getIZone();
        target=newtarget;
        setmoveTarget(new Point(target.x-(width+10),target.y-(height+10)));
        //System.out.println ("Entering addProduction() newUnit.getcost ().res1==" + newUnit.getcost ().res1 + " owner.getminerals ()= " + owner.getminerals ()); //Debug
        Building newBuilding=(Building)ClassLoad.load_string(newBuildingString,game,owner);
        if (owner.getminerals() < newBuilding.getcost().res1||que.size()!=0) //need check for resource 2**
            return;
        owner.takeminerals(newBuilding.getcost().res1);
        que.addLast(newBuilding);
        if (first) {
            countdownproduction = ((Building) que.getFirst()).getbuildspeed();
            //System.out.println(countdownproduction);//Debug
            first = false;
        }
    }
    
    public void removeProduction(int quenum){
        owner.addminerals(((GameObject)que.remove(quenum)).getcost().res1);
        first=true;
    }
    
    protected void production() {
        //System.out.println("| "+x+" - "+target.x+"|  "+(width+target.width+25)+"  "+y+" - "+target.y+"  "+(height+target.height+25));
        if (Math.abs(x-target.x)>=width+target.width+20||Math.abs(y-target.y)>=height+target.height+20){
            return;
        }
        countdownproduction--;
        //game.get_next_click_building_progresses().set(uplink_building_number,(int)(-100.0/(double)(((Building) que.getFirst()).getbuildspeed())*(double)countdownproduction+100.0));
        if (countdownproduction > 0)
            return;
        
        Building tempb = (Building) que.removeFirst();
        if(!create(tempb))
            owner.addminerals(tempb.getcost().res1);
        if (que.size() != 0) {
            countdownproduction = ((Building) que.getFirst()).getbuildspeed();
        }
        else
            first = true;
    }
    public IZone check_for_ghosts_and_set_uplink(){
        if(que.size()==0)
            return null;
        game.get_next_click_building_progresses().add((int)(-100.0/(double)(((Building) que.getFirst()).getbuildspeed())*(double)countdownproduction+100.0));
        target.setImage(((Building) que.getFirst()).getImageZone().getImage());
        return target;
    }
    
    public int getcurrentcount() {
        return countdownproduction;
    }
    
    public void act() {
    	//if(game.getDebug()) System.out.println("PU act-countdownproduction="+countdownproduction+"first="+first);
        if (que.size() != 0)
            production();
        super.act();
    }
    protected boolean create(Building newobj) {
        //System.out.println ("create " + newobj.fight);//Debug
        
        // Zone newzone = new Zone(x + width + 1, y + height + 1, newobj.width, newobj.height);
        if (game.isEmpty(target)) {
            newobj.add(target.getZone());
            return true;
        }
        return false;
    }
    
    
    public ArrayList getProductionItems() {
        return productionitems;
    }
    
    
    public LinkedList getque() {
        return que;
    }
}