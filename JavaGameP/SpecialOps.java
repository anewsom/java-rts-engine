package JavaGameP;

import java.awt.Image;
import java.util.ArrayList;
public class SpecialOps extends ProductionBuilding
{
        public SpecialOps(){
        width=100;
        height=100;
        fullhitpoints=700;
        hitpoints=700;
        name="SpecialOps";
        cost=new Resource(200, 0);
        clas=3;
        buildspeed=200;
        productionitems=new ArrayList();
        productionitems.add("Sniper");
        productionitems.add("Jedi");
        productionitems.add ("Upgrade Jedi Level + 1");
    }
    /**
     *	Obsolete contructor
     */
    public SpecialOps (Gamemap newgame, Zone z, Player newowner)
    {
	super (newgame, z, 700, "SpecialOps", newowner, new ArrayList (), new Resource (200, 0), 200);
	productionitems.add ("Sniper");
    productionitems.add ("Jedi");
    productionitems.add ("Upgrade Jedi Level + 1");
    }
}
