package JavaGameP;

import java.io.Serializable;

public class Direction implements Serializable
{
    private int dir;
    public Direction ()  // creates random direction
    {
	dir = (int) (Math.random () * 360);
    }


    public Direction (int newdir)
    {
	dir = newdir;
    }


    public boolean equals (Direction other)
    {
	return dir == other.getDirection ();
    }


    public int getDirection ()
    {
	return dir;
    }


    public Direction toRight (int degrees)
    {
	dir -= degrees;
	if (dir < 0)
	    dir += 360;
	return this;
    }


    public Direction toLeft (int degrees)
    {
	dir += degrees;
	if (dir > 360)
	    dir -= 360;
	return this;
    }
}
