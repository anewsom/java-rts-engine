package JavaGameP;

public class Paladin extends Tank
{
    public Paladin(){
        super();
        width=83;
        height=38;
        name="Paladin";
        movementrate=3;
        buildspeed=120;
        hitpoints=300;
        fullhitpoints=300;
       myweapon = new TankShell (this);
       attack_animation_counter=1;
    }
    public Paladin (Gamemap newgame, Zone z, Player newowner)
    {
	super (newgame, new Zone(z.x,z.y,83,38), 300, 3, "Paladin", newowner, 120, new Resource (200, 0),1, null);
	myweapon = new TankShell (this);
    }
}
