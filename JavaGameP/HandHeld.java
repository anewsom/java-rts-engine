package JavaGameP;


public class HandHeld extends Weapon{
    
    
    public HandHeld(FightingUnit newholder, int newattackdamage, int newdamagetype, int newrof, String newname, double newaccy) {
        super(newholder, newattackdamage, newdamagetype, newholder.getZone().width+5 , newrof, newname, newaccy);
    }
    
    public boolean inrange(GameObject target) {
        //System.out.println ((int) Math.sqrt (Math.pow (holder.getZone ().x - target.getZone ().x, 2) + Math.pow (holder.getZone ().y - target.getZone ().y, 2)));//Debug
        if ((int) Math.sqrt(Math.pow(holder.getCenter().x - target.getZone().x, 2) + Math.pow(holder.getCenter().y - target.getZone().y, 2)) <= range)
            return true;
        if((int) Math.sqrt(Math.pow(holder.getCenter().x - (target.getZone().x+target.getZone().width), 2) + Math.pow(holder.getCenter().y - target.getZone().y, 2)) <= range)
            return true;
        if((int) Math.sqrt(Math.pow(holder.getCenter().x - target.getZone().x, 2) + Math.pow(holder.getCenter().y - (target.getZone().y+target.getZone().height), 2)) <= range)
            return true;
        if((int) Math.sqrt(Math.pow(holder.getCenter().x - (target.getZone().x+target.getZone().width), 2) + Math.pow(holder.getCenter().y - (target.getZone().y+target.getZone().height), 2)) <= range)
            return true;
        
        if((int) Math.sqrt(Math.pow(holder.getCenter().x - target.getCenter().x, 2) + Math.pow(holder.getCenter().y - target.getZone().y, 2)) <= range)
            return true;
        if((int) Math.sqrt(Math.pow(holder.getCenter().x - target.getCenter().x, 2) + Math.pow(holder.getCenter().y - (target.getZone().y+target.getZone().height), 2)) <= range)
            return true;
        if((int) Math.sqrt(Math.pow(holder.getCenter().y - target.getCenter().y, 2) + Math.pow(holder.getCenter().x - target.getZone().x, 2)) <= range)
            return true;
        if((int) Math.sqrt(Math.pow(holder.getCenter().y - target.getCenter().y, 2) + Math.pow(holder.getCenter().x - (target.getZone().x+target.getZone().width), 2)) <= range)
            return true;
        if(holder.getZone().height < target.getZone().height){
            if((int) Math.sqrt(Math.pow(holder.getCenter().x - target.getZone().x, 2) + Math.pow(holder.getCenter().y - (target.getZone().y+holder.getZone().height), 2)) <= range)
                return true;
        }
        if(holder.getZone().width < target.getZone().width){
            if((int) Math.sqrt(Math.pow(holder.getCenter().y - target.getZone().y, 2) + Math.pow(holder.getCenter().x - (target.getZone().x+holder.getZone().width), 2)) <= range)
                return true;
        }
        return false;
    }
}
