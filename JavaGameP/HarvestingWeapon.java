package JavaGameP;

public class HarvestingWeapon extends HandHeld {
    protected int carry_counter,max_carry_counter=4;
    public HarvestingWeapon(Harvester newholder, int newattackdamage, int newrof, String newname) {
        super(newholder, newattackdamage, 4, newrof, newname, .8);
    }
    
    
    public void attack(GameObject target) {
                
        if (rofcounter == rof&& inrange(target)){
            carry_counter++;
            //holder.getowner ().addminerals (attackdamage);
            //((Harvester) holder).setcarryingMineralB(true);
            if(carry_counter==max_carry_counter+1){
            ((Harvester) holder).setReturnMineral(true);
            carry_counter=0;
            }
        }
        super.attack(target);
    }
    
    public int get_max_carry_counter(){
        return max_carry_counter;
    }
}
