package JavaGameP;

import java.util.*;
import java.awt.*;

public abstract class Unit extends GameObject {
    protected Point moveto;
    protected int movementrate, buildspeed;
    protected Direction mydir=new Direction(90);
    
    public Unit(){
        super();
        isunit=true;
        moveto = new Point(0,0);
    }
    
    public Unit(Gamemap newgame, Zone z, int newhitpoints, int newmovementrate, String newname, Player newowner, int newclas, int newbuildspeed, Resource newcost) {
        super(newgame, z, newhitpoints, newname, newowner, newclas, newcost, newbuildspeed);
        movementrate = newmovementrate;
        moveto = new Point(z.x, z.y);
        isunit = true;
    }
    
    public int getbuildspeed(){
        return buildspeed;
    }
    
    public void load(String [] loadstring,GameDisplay dis)throws NumberFormatException{
        super.load(loadstring,dis);
        moveto=new Point(Integer.parseInt(loadstring[6]),Integer.parseInt(loadstring[7]));
        mydir=new Direction(Integer.parseInt(loadstring[8]));
    }
    
    public String[] save(){
        String[]savestring=super.save();
        //line 6 = moveto.x
        savestring[6]=Integer.toString(moveto.x);
        //line 7 = moveto.y
        savestring[7]=Integer.toString(moveto.y);
        //line 8 = direction
        savestring[8]=Integer.toString(mydir.getDirection());
        
        return savestring;
    }
    
    
    public void act()  //called by Gamemap.step()
    {
        
        //  mydir = direct (x, y, moveto); // *****working, need to fix later*****
        // System.out.println(mydir.getDirection());//Debug
        move();
        
    }
    
    
    protected Direction direct(int nx, int ny, Point p) {
        int newangle = mydir.getDirection();
        int calcangle=0;
        if((p.x - nx)!=0)
            System.out.println((p.y - ny) / (p.x - nx));
        // calcangle = (int) Math.round(Math.atan((p.y - ny) / (p.x - nx)));
        if (p.x < nx && p.y > ny)
            newangle = 180 - calcangle;
        else if (p.x < nx && p.y < ny)
            newangle = 180 + calcangle;
        else if (p.x > nx && p.y > ny)
            newangle = calcangle;
        else if (p.x < nx && p.y > ny)
            newangle = 360 - calcangle;
        return new Direction(newangle);
    }
    
    
    public void setTarget(GameObject target) {
        moveto = target.location();
    }
    
    
    public void setmoveTarget(Point np) {
        // System.out.println ("In setmoveTarget"+np.x+" "+np.y);//Debug
        moveto = np;
        super.setmoveTarget(np);
    }
    protected void cancel_moving() {
        moveto=new Point(x,y);
    }
    
    /*
     *	Need to improve movement AI!!!
     */
    
    
    protected boolean move()  //called by act method, moves for one timestep
    {
        if (Math.abs(moveto.x - x) < movementrate && Math.abs(moveto.y - y) < movementrate)
            return false;
        boolean moved = false;
        if (moveto.x < x) {
            moveLeft();
            moved = true;
        }
        else if (moveto.x > x) {
            moveRight();
            moved = true;
        }
        if (moveto.y > y) {
            moveDown();
            moved = true;
        }
        else if (moveto.y < y) {
            moveUp();
            moved = true;
        }
        if (moved == false)
            cancel_moving();
        return moved;
    }
    
    
    protected boolean moveLeft() {
        Zone z = new Zone(x - movementrate, y, movementrate - 1, height);
        if (game.isEmpty(z)) {
            x = x - movementrate;
            return true;
        }
        // else
        //moveUp ();
        return false;
    }
    
    
    protected boolean moveRight() {
        Zone z = new Zone(x + width + movementrate, y, movementrate - 1, height);
        if (game.isEmpty(z)) {
            x = x + movementrate;
            return true;
        }
        // else
        //   moveDown ();
        return false;
    }
    
    
    protected boolean moveUp() {
        Zone z = new Zone(x, y - movementrate, width, movementrate - 1);
        if (game.isEmpty(z)) {
            y = y - movementrate;
            return true;
        }
        //else
        //  moveRight ();
        return false;
    }
    
    
    protected boolean moveDown() {
        Zone z = new Zone(x, y + height + movementrate, width, movementrate - 1);
        if (game.isEmpty(z)) {
            y = y + movementrate;
            return true;
        }
        //else
        //  moveLeft ();
        return false;
    }
}


