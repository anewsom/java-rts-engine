package JavaGameP;

public abstract class Building extends GameObject {
    
    public Building(){
        super();
        isbuilding=true;
        clas=3;
    }
    
    public Building(Gamemap newgame, Zone z, int newhitpoints, String newname, Player newowner, Resource newcost, int newbuildspeed) {
        super(newgame, z, newhitpoints, newname, newowner, 3, newcost, newbuildspeed);
        isbuilding=true;
    }
    
    
    public void act()  //working
    {
        super.act();
    }
}
