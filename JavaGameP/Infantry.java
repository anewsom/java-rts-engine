
package JavaGameP;

import java.awt.*;

public abstract class Infantry extends FightingUnit {
    public Infantry(){
	    super();
	    fullhitpoints=40;
	    hitpoints=40;
	    movementrate=5;
	    clas=1;
    }
    
    public Infantry(Gamemap newgame, Zone z, String newname, Player newowner, int newbuildspeed, Resource newcost,int newnum_attack_animation_frames, Weapon newweapon) {
        super(newgame, z, 40, 5, newname, newowner, 1, newbuildspeed, newcost,newnum_attack_animation_frames, newweapon);
    }
}