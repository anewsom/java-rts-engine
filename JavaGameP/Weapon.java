package JavaGameP;

import java.awt.Image;
import javax.swing.*;
import java.io.*;
import javax.sound.sampled.*;
import javax.sound.sampled.spi.AudioFileReader;
//import java.applet.*;
public class Weapon implements Cloneable, Serializable{
    protected int attackdamage, damagetype, range, rof, rofcounter = 0,fireVolume;
    protected double accy;
    protected int numFrames; //For the attack animation.
    protected transient Image attackImage;
    protected String name;
    protected transient FightingUnit holder;
    
    final static int Conncusive=1,Medium=2,Explosive=3,Normal=4,Siege=5,Sniper=6;
    
    public Weapon(FightingUnit newholder, int newattackdamage, int newdamagetype, int newrange, int newrof, String newname, double newaccy) {
        holder = newholder;
        attackdamage = newattackdamage;
        damagetype = newdamagetype;
        range = newrange;
        rof = newrof;
        name = newname;
        accy = newaccy;
        fireVolume=100;
    }
    public String getName(){
        return name;
    }
    
    public void setAttackImage(Image newAI) {
        attackImage = newAI;
    }
    
    
    public void setholder(FightingUnit newholder) {
        holder = newholder;
    }
    
    
    public Weapon createclone() {
        try {
            return (Weapon) this.clone();
        }
        catch (CloneNotSupportedException e) {
            System.out.println("CloneNotSupportedException in " + name);
            //assert false;
            return null;
        }
    }
    
    
    public int getattackdamage() {
        return attackdamage;
    }
    
    
    public int getrange() {
        return range;
    }
    
    private int volume(){
    	GameDisplay dis=holder.getgame().dis;
    	int finalVolume=fireVolume;
    	//sees if unit is on screen
    	if((holder.location().getX()<dis.vx||holder.location().getX()>(dis.vx+dis.resX()))||(holder.location().getY()<dis.vy||holder.location().getY()>(dis.vy+dis.resY())))
    	{
    	//sound reduced by distance... complex! Bugged, need to seperate and put in individual retrictions!
    	if(holder.location().getX()<dis.vx||holder.location().getX()>(dis.vx+dis.resX()))
    		finalVolume-=Math.abs(((double)Math.abs(holder.location().getX()-(dis.vx+dis.resX()/2))-dis.resX()/2)/10);
    	if(holder.location().getY()<dis.vy||holder.location().getY()>(dis.vy+dis.resY()))
    		finalVolume-=Math.abs(((double)Math.abs(holder.location().getY()-(dis.vy+dis.resY()/2))-dis.resY()/2)/10);
    	if(finalVolume<0)
    		return 0;
    	return finalVolume;
    	}
    	return fireVolume;
    }
    
    public void attack(GameObject target) {
        if (rofcounter == 1)
            holder.setImageback();
        if (rofcounter == rof && inrange(target)) {
            
            
            
					//sounds for weapons
                Clip tempclip;
                File tempfile = new File(name+".wav");
                AudioInputStream audioSource=null;
                FloatControl fc;
                try { try{ try{
                    audioSource = AudioSystem.getAudioInputStream(tempfile);
                    DataLine.Info info = new DataLine.Info(Clip.class, audioSource.getFormat());
                    tempclip = (Clip)AudioSystem.getLine(info);
                    tempclip.open(audioSource);
                    fc=(FloatControl) tempclip.getControl(FloatControl.Type.MASTER_GAIN);
                    
                    if(holder.getgame().getDebug())
                	holder.getgame().dis.debugger.println("Before set volume max:"+fc.getMaximum()+"min: "+ fc.getMinimum()+" and volume is "+volume());
                	
                	// conform volume percentage (0-100) to max value for fc
                    fc.setValue(((fc.getMaximum()+Math.abs(fc.getMinimum()))*((float)volume())/100)-Math.abs(fc.getMinimum()));
                    
					if(holder.getgame().getDebug())
                		holder.getgame().dis.debugger.println("After set Volume:"+fc.getValue());
                    tempclip.start();
                }
                catch (FileNotFoundException notThere){}
                }
                catch (UnsupportedAudioFileException notRight){
                	new ErrorGen(holder.getgame().dis,"UnsupportedAudioFileException",notRight);}
                }
                catch (Exception e) {
                    new ErrorGen(holder.getgame().dis,"SoundError",e);
                }
            
            
            
            
            target.dealdamage((int) Math.round(attackdamage * acc()), damagetype);
            holder.getgame().dis.attackAnimation(holder,target); //******attack animation (just need Image)
            if(target.getfight()&&((FightingUnit)target).isTargetNull())
            target.setTarget(holder);
            //System.out.println ("holder"); //debug
            holder.setattackImage();
            rofcounter = 0;
        }
        else if (rofcounter == rof)
            rofcounter = 0;
        if (!inrange(target)) {
            holder.setattackmoveTarget(target.location());
            //System.out.println ("!inrange " + target.location ().x + " " + target.location ().y);//Debug
        }
        rofcounter++;
    }
    
    
    private double acc() {
        double rand = Math.random();
      //  if(accy==1)
      //      return 1;
        return accy+rand*(1-accy);
      //  if (accy > rand && rand != 0)
       //     rand = acc();
       // return rand;
    }
    
    
    public boolean inrange(GameObject target) {
        //System.out.println ((int) Math.sqrt (Math.pow (holder.getZone ().x - target.getZone ().x, 2) + Math.pow (holder.getZone ().y - target.getZone ().y, 2)));//Debug
        if((int) Math.sqrt(Math.pow(holder.getCenter().x - target.getCenter().x, 2) + Math.pow(holder.getCenter().y - target.getCenter().y, 2)) <= range)
            return true;
        else
            return false;
    }
    
    
    public void upgradedamage(int bonus) {
        attackdamage += bonus;
    }
    
    
    public void upgraderange(int bonus) {
        range += bonus;
    }
}
