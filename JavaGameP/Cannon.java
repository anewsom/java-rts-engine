package JavaGameP;

public class Cannon extends Weapon
{
    public Cannon (Tank newholder, int newattackdamage, int newrange, int newrof, String newname)
    {
	super (newholder, newattackdamage, Weapon.Explosive, newrange, newrof, newname, .1);
    }
}
