package JavaGameP;
import java.awt.*;
import java.io.Serializable;
public class IZone implements Serializable{
	protected Gamemap game;
    public int x, y, height, width;
    protected transient Image im;
    
    public IZone(int newx, int newy, int newwidth, int newheight) {
        x = newx;
        y = newy;
        height = newheight;
        width = newwidth;
    }
    
    
    public IZone(Gamemap newgame, int newx, int newy, int newwidth, int newheight) {
        game = newgame;
        x = newx;
        y = newy;
        height = newheight;
        width = newwidth;
    }
	public IZone(Image newim, int newx, int newy, int newwidth, int newheight) {
        im = newim;
        x = newx;
        y = newy;
        height = newheight;
        width = newwidth;
    }
    public Image getImage() {
        return im;
    }
    public Zone getZone(){
    	return new Zone(x,y,height, width);
    }
        public boolean contains(Point p) {
        for (int w = 0 ; w < width ; w++) {
            for (int h = 0 ; h < height ; h++) {
                if (p.x > x && p.x < x + width && p.y > y && p.y < y + height)
                    return true;
            }
        }
        return false;
    }
    
    public void setImage(Image newim){
        im=newim;
    }
}