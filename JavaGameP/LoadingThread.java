package JavaGameP;
import java.awt.*;
class LoadingThread extends Thread{
    private GameDisplay dis;
    public boolean done=false,started=false,doner=false;
    
    public LoadingThread(GameDisplay newdis){
    	super();
        dis=newdis;
    }
    
    public void run(){
        int resX=dis.res().x,resY=dis.res().y;
        Graphics sg=dis.screen.getGraphics();
       Image offscreen=dis.createImage(resX,resY);
        Graphics gg=offscreen.getGraphics();
        gg.drawImage((Image)dis.Images.get("SWG"),0, 0, resX, resY,null);
        gg.setColor(Color.white);
        Font tempfont=new Font("Arial",Font.BOLD,25);
        gg.setFont(tempfont);
        gg.drawString("Loading.....", resX/2-60, resX/2);
        gg.drawRect(resX/4,3*resY/4,resX/2,50);
        started=true;
        sg.drawImage(offscreen,0,0,resX,resY,null);
        // this.yield();
        int loadtime=2;
        if(dis.qstart)
        loadtime=4;
        for (int d = 0 ; d < resX/2 ; d+=loadtime) {
            gg.fillRect(resX/4,3*resY/4,d,50);
           
            try{Thread.currentThread().sleep(75);}
        catch(InterruptedException e){new ErrorGen(null,"ThreadError",e);}
            sg.drawImage(offscreen,0,0,resX,resY,null);
           // this.yield();
        }        
        
        while(true){
        if(done)
            break;
        }
        doner=true;
    }
    
}
