package JavaGameP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.awt.*;
import javax.swing.*;
import java.io.*;
import javax.sound.sampled.*;

/**
 *	Gamemap
 *	The actual game engine--> Can be Serialized into a byte-code file for use in saving games
 *	contains the players.
 */
public class Gamemap implements Serializable{
    public transient GameDisplay dis;
    private ArrayList<GameObject> objects, selectedobj = new ArrayList<GameObject>(1), selobjsav1, selobjsav2, selobjsav3, selobjsav4, selobjsav5;
    private Point lastmouseclick, mouseclick, startdrag;
    private int fieldX,fieldY;
    private String next_click_building_string;
    private ArrayList<Integer> next_click_building_progresses= new ArrayList<Integer>(1);
    //private GameObject selectedobj;
    private transient HashMap<String,Image> Images;
    private ProductionUnit click_building;
    private boolean showdrawings, mdg = false, actiondrag,next_click_building=false; //shows moving circles and squares
    public String backgroundclipfilename,levelbackgroundname;
    public Player [] players = new Player [GameDisplay.maxPlayers+1];// = {gaia,one,two};
    public Player currentPlayer;
    
    public Gamemap(GameDisplay newdis) {
        dis = newdis;
    }
    
    
    public Gamemap(GameDisplay newdis, HashMap newImages) {
        dis = newdis;
        objects = new ArrayList<GameObject>();
                  
        Images = newImages;
    }
    public ArrayList<Integer> get_next_click_building_progresses()
    {
    	return next_click_building_progresses;
    }
    public void setImages(HashMap newImages){
    	Images= newImages;
    }
    public void setUpLoadedGame(GameDisplay newdis)
    {
    	dis=newdis;

    	GameObject temp;
    	if(objects!=null)
    	for(int c1=0;c1<objects.size();c1++)
    	{
    		temp =objects.get(c1);
    		GameObject tempSO;
    		if(selectedobj!=null)
    		for(int c2=0;c2<selectedobj.size();c2++)
    		{
    			tempSO =selectedobj.get(c2);
    			if(temp.equals(tempSO))
    			tempSO=temp;
    		}
    		GameObject tempSO2;
    		if(selobjsav1!=null)
      		for(int c3=0;c3<selobjsav1.size();c3++)
    		{
    			tempSO2 =selobjsav1.get(c3);
    			if(temp.equals(tempSO2))
    			tempSO2=temp;
    		}//need to add rest!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    		temp.setgame(this);//very needed, it does other stuff too!
    	}
    	for(Player tp:players)
    	{
    		if(currentPlayer.equals(tp))
    		currentPlayer=tp;
    		tp.setDis(newdis);
    	}
    }
    
    public void turnDebug(){
    	if(dis.debug)
    	dis.debug=false;
    	else
    	dis.debug=true;
    }
    public boolean getDebug(){
    	return dis.debug;
    }
    
    public void setGameObjectsToCurrentGame(){
        for(int d=0;d<objects.size();d++){
           objects.get(d).setgame(this);
        }
    }
    
    public void setfieldX(int newfieldX){
    	fieldX=newfieldX;
    }
    public void setfieldY(int newfieldY){
    	fieldY=newfieldY;
    }
    public int getfieldX(){
    	return fieldX;
    }
    public int getfieldY(){
    	return fieldY;
    }
    
    public void load_game(HashMap newImages, ArrayList<GameObject> savedobj) {
        objects = savedobj;
        Images = newImages;
    }
    
    
    public void add(GameObject newobj) {
        objects.add(newobj);
    }
    
    
    public void remove(GameObject oldobj) {
        objects.remove(oldobj);
    }
    
    
    public HashMap getImages() {
        return Images;
    }
    
    
    public void set_sel_attackmove(boolean set) {
        for (int d = 0 ; d < selectedobj.size() ; d++) {
            //if ( selectedobj.get(d).getfight() == true)
            if(selectedobj.get(d) instanceof FightingUnit)
                ((FightingUnit) selectedobj.get(d)).set_attack_move(set);
        }
        
    }
    
    
    public boolean contains(GameObject obj) {
        for (int d = 0 ; d < objects.size() ; d++) {
            if (objects.get(d).getID()==obj.getID())
                return true;
        }
        return false;
    }
    
    
    public void step(Point newmouseclick, Point movePoint, boolean mdown)throws CloneNotSupportedException {
        boolean end = true, endl = true;
        dis.showbackground();
        if (newmouseclick != null) {
            if (newmouseclick.equals(lastmouseclick)&&!dis.newmclick)
                mouseclick = null;
            else {
                mouseclick = newmouseclick;
                lastmouseclick = newmouseclick;
                dis.newmclick=false;
            }
        }
        //System.out.println (mdown + ", " + mdg); //debug
        if (mouseclick != null)
            actiondrag = checkselect();
        if (actiondrag && mdown && !mdg)
            startdrag = lastmouseclick;
        if (actiondrag && mdg && !mdown) {
            GameObject[] tempobj;
            if(selectedobj != null&&selectedobj.size()==0&&(movePoint.x - startdrag.x)>=0){
                tempobj = getplayerobjin(new Zone(startdrag.x, startdrag.y, movePoint.x - startdrag.x, movePoint.y - startdrag.y), currentPlayer);
            }
            else{
                tempobj = getplayerobjin(new Zone(movePoint.x, movePoint.y, startdrag.x-movePoint.x, startdrag.y-movePoint.y), currentPlayer);
            }
            if (tempobj != null) {
                if (selectedobj == null)
                    selectedobj = new ArrayList(1);
                    if(getDebug())dis.debugger.println ("tempobj.length="+tempobj.length);  //debug
                for (int d = 0 ; d < tempobj.length ; d++) {
                    selectedobj.add(tempobj [d]);
                }
            }
        }
        selobjexistsfixer(); // removes selected Objects when they no longer exist
        for (int d = 0 ; d < objects.size() ; d++) {
            if (end && ((GameObject) objects.get(d)).getowner().getnum() != currentPlayer.getnum() && ((GameObject) objects.get(d)).getowner().getnum() != 0) //new***
                end = false; //new***
            else if (endl && ((GameObject) objects.get(d)).getowner().getnum() == currentPlayer.getnum())
                endl = false;
                objects.get(d).act();
        }
        
        
        dis.displayObj(objects, selectedobj,next_click_building_progresses);
        if (showdrawings)
            showdrawings = dis.showmove();
        if (actiondrag && mdg && selectedobj !=null &&selectedobj.size()==0) {
            if((movePoint.x - startdrag.x)>=0)
                dis.showselrect(startdrag, movePoint);
            else
                dis.showselrect(movePoint, startdrag);
        }
       /* targetc = dis.gettargetsc ();
        * //if(targetc!=null
        * for (int d = 0 ; d < targetc.size () ; d++)
        * {
        *     //if (targetc.get (d) != null)
        *     dis.attackAnimation ((GameObject) targetc.get (d), d);
        * }
        * dis.incrementcounters();
        */
        
        mdg = mdown;
        if (end)
            endGame(1);
        if (dis.autodefeat||endl)
           endGame(2);
    }
    
    
    public void endGame(int d) {
        if (d == 1)
            dis.showVictory();
        if (d == 2)
            dis.showDefeat();
    }
    
    
    public boolean isEmpty(Zone zone) {
        for (int d = 0 ; d < objects.size() ; d++) {
            if (objects.get(d).contains(zone))
                return false;
        }
        return true;
    }
   public boolean isEmpty(IZone zone) {
        for (int d = 0 ; d < objects.size() ; d++) {
            if (objects.get(d).contains(zone))
                return false;
        }
        return true;
    }
    
    
    public GameObject[] getobjin(Zone nz) {
        LinkedList<GameObject> list = new LinkedList<GameObject>();
        for (int d = 0 ; d < objects.size() ; d++) {
            if (objects.get(d).contains(nz))
                list.addLast(objects.get(d));
        }
        if (list.size() == 0)
            return new GameObject [0];
        GameObject[] re = new GameObject [list.size()];
        for (int d = 0 ; d < list.size() ; d++) {
            re [d] = list.get(d);
        }
        return re;
    }
    
    
    public GameObject[] getplayerobjin(Zone nz, Player player) {
        LinkedList<GameObject> list = new LinkedList<GameObject>();
        for (int d = 0 ; d < objects.size() ; d++) {
            if (objects.get(d).contains(nz) && objects.get(d).getowner().getnum() == player.getnum())
                list.addLast(objects.get(d));
        }
        if (list.size() == 0)
            return null;
        GameObject[] re = new GameObject [list.size()];
        for (int d = 0 ; d < list.size() ; d++) {
            re [d] = list.get(d);
        }
        return re;
    }
    
    
    public boolean isEmpty(Point p) {
        for (int d = 0 ; d < objects.size() ; d++) {
            if (objects.get(d).contains(p))
                return false;
        }
        return true;
    }
    
    public GameObject getObj(int IDfind){
        for (int d = 0 ; d < objects.size() ; d++) {
            if ( objects.get(d).getID()==IDfind)
                return objects.get(d);
        }
        return null;
    }
    
    
    private void selobjexistsfixer() {
        if (selectedobj != null && selectedobj.size() != 0)
            for (int d = selectedobj.size() - 1 ; d >= 0 ; d--) {
                if (!selectedobj.get(d).exists())
                    selectedobj.remove(d);
            }
    }
    
    
    private boolean checkselect() {
        if(next_click_building&&mouseclick.x<dis.res().x-150){
            if(!isEmpty(new Zone(mouseclick.x,mouseclick.y,100,100)))
                return false;
            next_click_building=false;
            if(getDebug())dis.debugger.println("in nextclick");//Debug
            dis.set_building_location(null,false);
            click_building.addProduction(next_click_building_string,new Zone(mouseclick.x,mouseclick.y,100,100));
        }
        else if (!isEmpty(mouseclick))
            return doobj(mouseclick);
        else if (selectedobj != null && selectedobj.size() != 0) {
            if((!(selectedobj.get(0) instanceof ProductionBuilding)&&!(selectedobj.get(0) instanceof ProductionUnit))||( selectedobj.get(0)instanceof ProductionUnit&&mouseclick.x+dis.vx<dis.res().x-150)){
                for (int selobjcount = 0 ; selobjcount < selectedobj.size() ; selobjcount++) //multiple selobj
                {
                    selectedobj.get(selobjcount).setmoveTarget(mouseclick);
                }
                dis.showmove(mouseclick, 1);
                showdrawings = true;
            }
            if ( selectedobj.get(0)instanceof ProductionBuilding||selectedobj.get(0)instanceof ProductionUnit) {
                if (!checknewproduction())
                    deselect(0);
            }
        }
        return true;
    }
    
    
    private boolean checknewproduction() {
        if(selectedobj.get(0)instanceof ProductionBuilding){
            ProductionBuilding selectedpro = (ProductionBuilding) selectedobj.get(0);
            
            int decy = 20;
            for (int d = 0 ; d < selectedpro.getProductionItems().size() ; d++) {
                if (new Zone(900 + dis.vx, decy + 75 * d + dis.vy, 60, 60).contains(mouseclick)) {
                    selectedpro.addProduction((String)selectedpro.getProductionItems().get(d));
                    return true;
                }
            }
            decy = dis.res().y - 100;
            for(int d = 0 ; d < selectedpro.getque().size() ; d++) {
                if(new Zone(900 + dis.vx, decy + dis.vy, 60, 60).contains(mouseclick))
                    selectedpro.removeProduction(d);
                decy -= 75;
            }
        }
        else{
            ProductionUnit selectedpro = (ProductionUnit) selectedobj.get(0);
            
            int decy = 20;
            for (int d = 0 ; d < selectedpro.getProductionItems().size() ; d++) {
                if(selectedpro.getque().size()==1)
                    break;
                if (new Zone(900 + dis.vx, decy + 75 * d + dis.vy, 60, 60).contains(mouseclick)) {
                    //System.out.println(900 + dis.vx+" "+decy + 75 * d + dis.vy);
                    //System.out.println(selectedpro.getProductionItems().size());
                    next_click_building_string =(String) selectedpro.getProductionItems().get(d);
                    dis.set_building_location(next_click_building_string,true);
                    
                    next_click_building=true;
                    click_building=selectedpro;
                    //click_building.setuplink_building_number(next_click_building_progresses.size());
                    //next_click_building_progresses.add(0);
                    // System.out.println("in selectnewPro");//Debug
                    //selectedpro.cancel_moving();
                    deselect();
                    return true;
                }
            }
            decy = dis.res().y - 100;
            for(int d = 0 ; d < selectedpro.getque().size() ; d++) {
                if(new Zone(900 + dis.vx, decy + dis.vy, 60, 60).contains(mouseclick)){
                    selectedpro.removeProduction(d);
                    next_click_building=false;
                }
                decy -= 75;
            }
            return true;
        }
        
        return false;
    }
    
    
    private boolean doobj(Point p)  // selects objects
    {
        GameObject temp;
        for (int d = 0 ; d < objects.size() ; d++) {
            temp = objects.get(d);
            if (temp.contains(p) && temp.getowner().getnum() == currentPlayer.getnum()) {
                selectedobj = new ArrayList<GameObject>(1);
                selectedobj.add(temp);
                return true;
            }
            else if (temp.contains(p) && selectedobj != null && selectedobj.size() != 0) {
                // System.out.println ("Entering selectobj()  if (temp.contains (p) && selectedobj != null) "); //Debug
                for (int selobjcount = 0 ; selobjcount < selectedobj.size() ; selobjcount++) //multiple selobj
                {
                   selectedobj.get(selobjcount).setTarget(objects.get(d));
                }
                dis.showmove(p, 2);
                showdrawings = true;
                return false;
            }
        }
        return true;
    }
    
    
    public boolean selobjsave(int kn) {
        if (kn == 33) //save 1
            selobjsav1 = selectedobj;
        else if (kn == 64) //save 2
            selobjsav2 = selectedobj;
        else if (kn == 35) //save 3
            selobjsav3 = selectedobj;
        else if (kn == 36) //save 4
            selobjsav4 = selectedobj;
        else if (kn == 37) //save 5
            selobjsav5 = selectedobj;
        else if (kn == 49) //recl 1
            selectedobj = selobjsav1;
        else if (kn == 50) //recl 2
            selectedobj = selobjsav2;
        else if (kn == 51) //recl 3
            selectedobj = selobjsav3;
        else if (kn == 52) //recl 4
            selectedobj = selobjsav4;
        else if (kn == 53) //recl 5
            selectedobj = selobjsav5;
        return false;
    }
    
    public void stopsel() {
        if (selectedobj != null)
            for (int d = 0 ; d < selectedobj.size() ; d++) {
                if(selectedobj.get(d)instanceof Unit)
                    ((Unit) selectedobj.get(d)).cancel_moving();
            }
    }
    public boolean get_next_click_building(){
        return next_click_building;
    }
    
    public void killsel() {
        if (selectedobj != null)
            for (int d = 0 ; d < selectedobj.size() ; d++) {
               selectedobj.get(d).die();
            }
    }
    public void returndroneswithminerals(){
        if (selectedobj != null&&selectedobj.get(0)instanceof Harvester)
            ((Harvester)selectedobj.get(0)).redoReturnTarget();
    }
    
    public void deselect() {
        selectedobj = new ArrayList<GameObject>(1);
    }
    
    public void deselect(int index) {
        selectedobj.remove(index);
    }
    public void save_game(){
        String save_path;
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Save To");
        chooser.setApproveButtonText("Save To");
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setCurrentDirectory(new File("Saved Games"));
        int returnVal = chooser.showOpenDialog(dis);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            save_path=chooser.getSelectedFile().getAbsolutePath();
        }
        else
            save_path="Saved Games\\SaveXXX.sav";
            
            ObjectOutputStream fileOut;
            
            try{
            fileOut=new ObjectOutputStream(new FileOutputStream(save_path));
            fileOut.writeObject(this);
            fileOut.close();
            }catch(IOException IOE){new ErrorGen(dis,"IOE",IOE);}
            }
}


