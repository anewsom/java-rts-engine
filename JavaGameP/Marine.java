package JavaGameP;

import java.awt.*;
import java.util.*;
public final class Marine extends Infantry {
    public Marine(){
        super();
        width=28;
        height=28;
        name="Marine";
        //cost=new Resource(40, 0);
        buildspeed=35;
        myweapon = new LightMachineGun(this);
        attack_animation_counter=1;
        //System.out.println("+"+buildspeed+" "+getbuildspeed());
        
    }

    
    public Marine(Gamemap newgame, Zone z, Player newowner) {
        super(newgame, new Zone(z.x,z.y,28,28), "Marine", newowner, 35, new Resource(40, 0),1, null);
        myweapon = new LightMachineGun(this);
    }
}
