package JavaGameP;

import java.awt.Image;

public class Sniper extends Infantry
{
    public Sniper(){
        super();
        width=27;
        height=27;
        name="Sniper";
        buildspeed=50;
        myweapon = new SniperRifle(this);
        attack_animation_counter=1;
    }
    
    public Sniper (Gamemap newgame, Zone z, Player newowner)
    {
	super (newgame, new Zone(z.x,z.y,27,27), "Sniper", newowner, 50, new Resource (120, 0),1, null);
	myweapon = new SniperRifle (this);
    }
}
