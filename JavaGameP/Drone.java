package JavaGameP;

public class Drone extends Harvester {
    public Drone(){
        super();
        width=25;
        height=25;
        name="Drone";
        cost=new Resource(50, 0);
        buildspeed=35;
        hitpoints=60;
        fullhitpoints=60;
        myweapon = new FusionCutter(this);
        movementrate=8;
		attack_animation_counter=1;
    }
    public Drone(Gamemap newgame, Zone z, Player newowner) {
        super(newgame,new Zone(z.x,z.y,25,25), 60, 8, "Drone", newowner, 35, new Resource(50, 0),1, null);
        myweapon = new FusionCutter(this);
    }
}
