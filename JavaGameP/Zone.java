package JavaGameP;
import java.awt.*;
import java.io.Serializable;
public class Zone implements Serializable {
    protected Gamemap game;
    public int x, y, height, width;
    //protected Image im;
    
    public Zone(int newx, int newy, int newwidth, int newheight) {
        x = newx;
        y = newy;
        height = newheight;
        width = newwidth;
    }
    
    
    public Zone(Gamemap newgame, int newx, int newy, int newwidth, int newheight) {
        game = newgame;
        x = newx;
        y = newy;
        height = newheight;
        width = newwidth;
    }
    public IZone getIZone()
    {
    	return new IZone(x,y,width,height);
    }
    
    
    public boolean contains(Point p) {
        for (int w = 0 ; w < width ; w++) {
            for (int h = 0 ; h < height ; h++) {
                if (p.x > x && p.x < x + width && p.y > y && p.y < y + height)
                    return true;
            }
        }
        return false;
    }
    
    
    public Gamemap game() {
        return game;
    }
}
