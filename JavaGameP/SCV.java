package JavaGameP;

import java.util.ArrayList;
import java.util.LinkedList;

public class SCV extends ProductionUnit {
    public SCV(){
        super();
        clas=2;
        width=25;
        height=25;
        fullhitpoints=60;
        hitpoints=60;
        //cost=new Resource(50, 0);
        buildspeed=35;
        name="SCV";
        movementrate=2;
       productionitems= new ArrayList();
        productionitems.add("SpecialOps");
        productionitems.add("Barracks");
        productionitems.add("Shop");
        productionitems.add("Base");
        builder=true;
    }
    public SCV(Gamemap newgame,Zone z,Player newowner) {
        super(newgame, new Zone(z.x,z.y,25,25), 60,  3, "SCV",  newowner, 3, 35, new Resource(50, 0), new ArrayList());
        productionitems.add("SpecialOps");
        productionitems.add("Barracks");
        productionitems.add("Shop");
        productionitems.add("Base");
        
        builder=true;
    }
    
}
