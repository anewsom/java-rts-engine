package JavaGameP;

import java.awt.Image;

public class Jedi extends Infantry {
    protected int Jedi_level=1;
    public Jedi(){
        super();
        width=23;
        height=28;
        buildspeed=40;
        name="Jedi";
        myweapon = new LightSaber(this);
        attack_animation_counter=1;
        isJedi=true;
        movementrate=8;
    }
    public Jedi(Gamemap newgame, Zone z, Player newowner) {
        super(newgame,new Zone(z.x,z.y,30,37), "Jedi", newowner, 35, new Resource(100, 0),1, null);
        myweapon = new LightSaber(this);
        movementrate=8;
        isJedi=true;
        Jedi_level=newowner.getJediLevel();
    }
    
     
    //damagetype(StarCraft) -> 1=conncusive, 2=medium,3=explosive,4=normal,5=siege, 6=sniper
    public void dealdamage(int attackdamage, int damagetype) {
        //System.out.println ("Starting GameObject.dealdamage() attackdamage = " + attackdamage + " damagetype = " + damagetype); //Debug
        int realdamage = 0;
        switch (damagetype) {
            case 1:
                realdamage += attackdamage;
                break;
            case 2:
                realdamage += attackdamage / 2;
                break;
            case 3:
                realdamage += attackdamage / 1.5;
                break;
            case 4:
                realdamage += attackdamage;
                break;
            case 5:
                realdamage += attackdamage / 4;
                break;
            case 6:
                realdamage += attackdamage;
                break;
            default:
        } //switch
        if(hitpoints>=realdamage/2&&(Math.random()/(double)Jedi_level)<.2){
        	
            realdamage=realdamage/15;
            }
        hitpoints -= realdamage;
        
        if (hitpoints <= 0 && exists()) {
            die();
        }
    }
    public void set_Jedi_level(int new_level){
        Jedi_level=new_level;
    }
}
