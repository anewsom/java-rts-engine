package JavaGameP;

public class LightMachineGun extends MachineGun {
    public LightMachineGun(FightingUnit newholder) {
        super(newholder, 20, "LightMachineGun");
        fireVolume=75;
    }
}
