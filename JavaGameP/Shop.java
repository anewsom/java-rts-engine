package JavaGameP;

import java.awt.Image;
import java.util.ArrayList;
public class Shop extends ProductionBuilding
{
        public Shop(){
        width=100;
        height=100;
        fullhitpoints=700;
        hitpoints=700;
        name="Shop";
        cost=new Resource(350, 0);
        buildspeed=200;
        productionitems=new ArrayList();
        productionitems.add("Paladin");
    }
    public Shop(Gamemap newgame, Zone z, Player newowner)
    {
	super (newgame, z, 700, "Shop", newowner, new ArrayList (), new Resource (350, 0), 200);
	productionitems.add ("Paladin");
    }
}
