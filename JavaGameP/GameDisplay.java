package JavaGameP;


import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashMap;
import javax.swing.*;
import javax.imageio.*;
import javax.sound.sampled.*;
import java.io.*;
import java.net.*;

/**
 *	GameDisplay
 *	The Game-->Menus and display controls, not the game engine
 *	When multiplayer is realized, this part of the program will run on the client computer
 */
public class GameDisplay extends JFrame implements WindowListener {
    public static boolean onerun;
    public static GameDisplay Display;
    public GraphicsDevice gs = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0];
    public Canvas screen = new Canvas();
    public PrintWriter debugger;
    public long gamecount = 0;
    private int resX, resY,/* fieldX = 1500, fieldY=1200,*/ credc=0,levelpickI=0,savedgamenum,level_pick_color_I=0,currentPlayerInc_i=1; //screen resolution & battlefield offscreen
    private Color [] level_pick_colors = {Color.BLUE,Color.RED,Color.MAGENTA,Color.YELLOW,Color.GRAY,Color.BLACK,Color.CYAN,Color.ORANGE,Color.WHITE,Color.PINK};
    public int vx = 0, vy = 0;
    public static final int maxUnits = 20,maxPlayers=2; //maxUnits
    private Point p = new Point(0, 0), mp = new Point(0, 0), mpr = new Point(0, 0), movepoint; //movepoint use in drawing move circles
    private int w, type, counter = 0; //drawing move circles and other things (counters)***
    //private int[] attackcircles = new int [100];
    private Image backgroundM, offscreen,offscreenM, currenti, temp,building_ghost; // currenti for temp use
    public Gamemap g;
    private String backgroundMusicFileName;
	public Object [] settings = new Object [10];//coded 10 settings retriction: 0-menu music 0=on; 1-debug 1=on; 2-in game music 0=on
    private IZone currentz; //temp use
    private boolean firstload = true, mdown = false,launchsp =false,launchmp=false,launchmps=false,showcr =false,showop=false, menub=true, gamerun=false,levelpickb=false,level_next_click=false,loadsp=false,levelloadb=false,building_ghost_b=false,attack_ani_b=true,show_save_b=false,level_pick_color_inc_b=false,time_to_save=false,levelPlayerSideInc_b=false,exitCurrentGame=false;
    public boolean autodefeat=false, forcepause=false, selunpause=false,newmclick=false, qstart=true,debug=false;
    public ProductionBuilding embase;
    private Clip menuclip,backgroundclip;
    private File file_to_be_loaded;
    public String levelbackgroundname;
    private ArrayList Levels=new ArrayList();
    public HashMap<String,Image> Images = new HashMap<String,Image>(maxUnits); /*Map of Images places images in hashmap*/
    public HashMap Sounds = new HashMap(maxUnits);
    public HashMap<String,ArrayList> AbilityList=new HashMap<String,ArrayList>(maxUnits);
    public HashMap<String,Resource> Costs=new HashMap(maxUnits);
    
    public GameDisplay() throws InterruptedException, CloneNotSupportedException {
        super("Window Wars");  // Set the frame's name
        Display=this;
        setIgnoreRepaint(true);
        
        /*
         *	Debugging file creation!!!
         */
        File debugFile=new File("Debug.txt");
        try{
       	debugFile.createNewFile();
        debugger= new PrintWriter(debugFile);
        }catch(Exception HELP){new ErrorGen(this,"Debug-File Writer",HELP);}
        
        //	Settings File Read:
        BufferedReader settingsReader;
        File settingFile=new File("Settings.ww");
        if(!settingFile.exists()){
        	try{settingFile.createNewFile();}
        	catch(IOException BAD){new ErrorGen(this,"Settings-File create empty file",BAD);}
        for(int d=0;d<10;d++) 
        settings[d]="0";
        }
        else
        try{try{
        settingsReader=new BufferedReader(new FileReader("Settings.ww"));
        for(int d=0;d<10;d++)
        settings[d]=settingsReader.readLine();
        }catch (FileNotFoundException BAD){new ErrorGen(this,"Settings-File reader",BAD);}
        }catch (Exception HELP){new ErrorGen(this,"Settings-File reader",HELP);}
        
        //checks the settings file to make sure it will work
        for(int d=0;d<10;d++)
        	if(settings[d]==null)
        		settings[d]="0";
        		
        if(settings[1].equals("1"))
        	debug=true;
        
        setUndecorated(true);
        resX=1028;
        resY=768-30;
        addWindowListener(this);
        setResizable(false);
        
        if(gs!=null) //Switching to fullscreen mode with resolution change!!
        {
			gs.setFullScreenWindow(this);
	      	if(debug)
        		debugger.println(gs.isDisplayChangeSupported());
			if(gs.isDisplayChangeSupported())
       			gs.setDisplayMode(new DisplayMode (1024,768,32,60));
       	}
        
        setLayout(new BorderLayout()); //allows canvas to be put on frame
        getContentPane().add("Center", screen); //puts canvas on frame
                
        LoadingThread lt=new LoadingThread(this);
        try{
            temp=ImageIO.read(new File("SWG screen shot.jpg"));
            prepareImage(temp, screen);
            Images.put("SWG", temp);
	            try{
	                AudioInputStream audioSource = AudioSystem.getAudioInputStream(new File("backgroundmenu.wav"));
	                DataLine.Info info = new DataLine.Info(Clip.class, audioSource.getFormat());
	                menuclip = (Clip)AudioSystem.getLine(info);
	                menuclip.open(audioSource);
	                if(settings[0].equals ("0"))
	                menuclip.loop(menuclip.LOOP_CONTINUOUSLY);
            		}
            catch (UnsupportedAudioFileException e) {new ErrorGen(this,"Sound-UnsupportedAudioFileException",e);}
        }catch(Exception errorIO){new ErrorGen(this,"Sound-IOException",errorIO);}
        
        setVisible(true); // Show the frame
        lt.start();
        
        
        //------------------------------------------------- File Readins
        String tempcheck1;
        
        BufferedReader images_in_menu;//,images_in_game;
        try{try{try{
            images_in_menu = new BufferedReader(new FileReader("ImageListMenu.ww"));
            
            temp = getToolkit().getImage(images_in_menu.readLine());
            prepareImage(temp, screen);
            backgroundM = temp;
            
            temp = getToolkit().getImage(images_in_menu.readLine());
            prepareImage(temp, screen);
            Images.put(images_in_menu.readLine(), temp);
            setIconImage(temp);
            
            temp = getToolkit().getImage(images_in_menu.readLine());
            setCursor(getToolkit().createCustomCursor(temp, new Point(Integer.parseInt(images_in_menu.readLine()), Integer.parseInt(images_in_menu.readLine())), images_in_menu.readLine()));
            
             while(true){
                tempcheck1=images_in_menu.readLine();
                if(tempcheck1.equalsIgnoreCase("X"))
                    break;
                temp = getToolkit().getImage(tempcheck1);
                prepareImage(temp, screen);
                Images.put(images_in_menu.readLine(), temp);
            }
            /*
            images_in_game=new BufferedReader(new FileReader("ImageList.ww"));
            while(true){
                tempcheck1=images_in_game.readLine();
                if(tempcheck1.equalsIgnoreCase("X"))
                    break;
                temp = getToolkit().getImage(tempcheck1);
                prepareImage(temp, screen);
                Images.put(images_in_game.readLine(), temp);
            }*/
            
        }catch(NumberFormatException e){new ErrorGen(this,"Images-NumberFormatException",e);}
        }catch(FileNotFoundException e){new ErrorGen(this,"ImageList.ww Not Found",e);while(true){}}
        }catch(IOException e){new ErrorGen(this,"Image-IOException",e);}
        
        BufferedReader levels_in;
        try{
            levels_in = new BufferedReader(new FileReader("Levels.ww"));
        }catch(FileNotFoundException e){new ErrorGen(this,"Levels.ww Not Found",e);while(true){}}
        try{
            while(true){
                tempcheck1=levels_in.readLine();
                if(tempcheck1.equalsIgnoreCase("X"))
                    break;
                Levels.add(tempcheck1);
            }
        }catch(IOException e){new ErrorGen(this,"IOExecption-Levels.ww",e);}
        
        BufferedReader costs_in;
        try{
            costs_in = new BufferedReader(new FileReader("CostList.ww"));
        }catch(FileNotFoundException e){new ErrorGen(this,"CostList.ww Not Found",e);while(true){}}
        try{
            while(true){
                tempcheck1=costs_in.readLine();
                if(tempcheck1.equalsIgnoreCase("X"))
                    break;
                Costs.put(tempcheck1,new Resource(Integer.parseInt(costs_in.readLine()),Integer.parseInt(costs_in.readLine())));
            }
        }catch(IOException e){new ErrorGen(this,"IOExecption-CostsList.ww",e);}
        
                BufferedReader ability_in;
        try{
            ability_in = new BufferedReader(new FileReader("AbilityList.ww"));
        }catch(FileNotFoundException e){new ErrorGen(this,"AbilityList.ww Not Found",e);while(true){}}
        try{
            while(true){
                tempcheck1=ability_in.readLine();
                if(tempcheck1.equalsIgnoreCase("X"))
                    break;
                ArrayList abilities= new ArrayList(1);
                while(true)
                {
                	String tempcheck2=ability_in.readLine();
                	if(tempcheck2.equals("N"))
                	break;
                	abilities.add(tempcheck2);
                }
                AbilityList.put(tempcheck1,abilities);
            }
        }catch(IOException e){new ErrorGen(this,"IOExecption-AbilityList.ww",e);}
        
        //--------------------------- end File ReadIns
        lt.done=true;
        while(true){
            if(lt.doner)
                break;
        }
    } // Constructor
    
    private void GameControl() {
        File loadFile;
        while(true){
        	exitCurrentGame=false;
        	if(settings[0].equals("0")&&!menuclip.isRunning())
            	menuclip.loop(menuclip.LOOP_CONTINUOUSLY);
        
        int levelint=0;
        while(true){
            if(levelint==0)
                levelint= menu();
            else
                break;
        }
        menub=false;
        //load game images
        String tempcheck1;
        BufferedReader images_in_game;
        try{
        
                    images_in_game=new BufferedReader(new FileReader("ImageList.ww"));
            while(true){
                tempcheck1=images_in_game.readLine();
                if(tempcheck1.equalsIgnoreCase("X"))
                    break;
                temp = getToolkit().getImage(tempcheck1);
                prepareImage(temp, screen);
                Images.put(images_in_game.readLine(), temp);
            }
            }
            catch(IOException IOE){new ErrorGen(this,"IOE",IOE);}
            
        if(levelint!=-1)
        {
            g = new Gamemap(this, Images); // starts game enviroment
            
            for(int local=0;local<3;local++)//sets up players
            g.players[local]=new Player(local);
        
            for(int temp1=0;temp1<3;temp1++){
            g.players[temp1].setDis(this);
        	}
       
	        g.currentPlayer=g.players[currentPlayerInc_i];
	        g.load_game(Images, level(g,levelint)); // need to overhaul the way normal (not loaded) games are started so You can start with more options for games that like 3 right now and like a max of 6 or 7
	        g.levelbackgroundname=levelbackgroundname;
	        g.backgroundclipfilename=backgroundMusicFileName;
        }
        else
        {
        	level(file_to_be_loaded);
        	//debugger.println("else "+(g==null));//Debug
        	g.setImages(Images);//fixed
        	g.setUpLoadedGame(this);
        	
        	try {
          			File tempfile = new File(g.backgroundclipfilename);
                    AudioInputStream audioSource = AudioSystem.getAudioInputStream(tempfile);
                    DataLine.Info info = new DataLine.Info(Clip.class, audioSource.getFormat());
                    backgroundclip = (Clip)AudioSystem.getLine(info);
                    backgroundclip.open(audioSource);
                    
            }catch (UnsupportedAudioFileException e) {new ErrorGen(this,"Sound-UnsupportedAudioFileException",e);}
             catch(LineUnavailableException errorl){new ErrorGen(this,"Sound-LineUnavailableException",errorl);}
             catch(IOException errorIO){new ErrorGen(this,"Sound-IOException",errorIO);}
        }
                              
        /*if(levelint==-1){
            loadFile= file_to_be_loaded;
            try{try{
                g.load_game(Images, level(loadFile));
            }catch (IOException IOE){new ErrorGen(this,"IOE",IOE);}
            }catch (NumberFormatException NFErr){new ErrorGen(this,"NFE",NFErr);}
            //g.setGameObjectsToCurrentGame();
        }*/
        
        menuclip.stop();
        if(settings[2].equals("0"))
        	backgroundclip.loop(backgroundclip.LOOP_CONTINUOUSLY);
        	
        g.players[0].setColor(Color.GREEN);
        g.currentPlayer.setColor(level_pick_colors[level_pick_color_I]);
        int otherNum=2;
        if(g.currentPlayer.getnum()==2)
        otherNum=1;
        if(level_pick_color_I+1==level_pick_colors.length)
            g.players[otherNum].setColor(level_pick_colors[0]);
        else
            g.players[otherNum].setColor(level_pick_colors[level_pick_color_I+1]);
        
        try{
            Thread.currentThread().sleep(1000); //load delay
            run();
        } catch(InterruptedException er1){new ErrorGen(this,"ThreadError",er1);}
         catch(CloneNotSupportedException er2){new ErrorGen(this,"CloneError",er2);}
        }
    }
    
    
    private int menu() {
        menub=true;
        gamerun=false;
        firstload = true;
        launchsp =false;
        launchmp =false;
        launchmps =false;
        showcr =false;
        loadsp=false;
        show_save_b=false;
        
        try{Thread.currentThread().sleep(1000);}
        catch(InterruptedException e){new ErrorGen(this,"ThreadError",e);}
        
        Graphics real,unreal;
        offscreenM = this.createImage(resX, resY);
        while(true){
            real = screen.getGraphics();
            //offscreenM = this.createImage(resX, resY);
            unreal = offscreenM.getGraphics();
            draw_background();
            if(showcr){//unreal.fillRect(7,420,222,446-392); //MultiPlayer
                unreal.setColor(Color.BLUE);
                unreal.fillRect(7,480,222,446-392);
                real.drawImage(offscreenM, 0, 0, resX+5, resY, this);
                try{Thread.currentThread().sleep(200); //load delay
                } catch(InterruptedException er1){new ErrorGen(this,"ThreadError",er1);}
                show_Credits();
            }
            if(show_save_b){
                unreal.setColor(Color.BLUE);
                unreal.fillRect(250,422,435-250,50);//x>250&&y>450&&x<435&&y<500
                real.drawImage(offscreenM, 0, 0, resX+5, resY, this);
                try{Thread.currentThread().sleep(200); //load delay
                } catch(InterruptedException er1){new ErrorGen(this,"ThreadError",er1);}
                if(saved_games_page())
                    return -1;
            }
            if(showop){
                unreal.setColor(Color.BLUE);
                unreal.fillRect(250,537,435-250,610-566);
                real.drawImage(offscreenM, 0, 0, resX+5, resY, this);
                try{Thread.currentThread().sleep(200); //load delay
                } catch(InterruptedException er1){new ErrorGen(this,"ThreadError",er1);}
                option_page();
                showop=false;
            }
            if(launchsp){
                unreal.setColor(Color.BLUE);
                unreal.fillRect(7,365,222,446-392);
                real.drawImage(offscreenM, 0, 0, resX+5, resY, this);
                try{Thread.currentThread().sleep(200); //load delay
                } catch(InterruptedException er1){new ErrorGen(this,"ThreadError",er1);}
                return picklevel();
            }
                if(launchmp){
                unreal.setColor(Color.BLUE);
                unreal.fillRect(7,415,222,446-392);
                real.drawImage(offscreenM, 0, 0, resX+5, resY, this);
                try{Thread.currentThread().sleep(200); //load delay
                } catch(InterruptedException er1){new ErrorGen(this,"ThreadError",er1);}
                return multiplayer_page();
            }
            
            levelpickb=false;
            real.drawImage(offscreenM, 0, 0, resX+5, resY, this);
        }
    }
    
    private int multiplayer_page() {
    	Graphics real,unreal;
    	int socketnum=666;
    	
    	//will fix this!!!
    //	try{
    //	DatagramSocket s = new DatagramSocket(socketnum);
    //	}catch (SocketException se){new ErrorGen(this,"Network Error",se);} 
    	
        while(true){
            real = screen.getGraphics();
            unreal = offscreenM.getGraphics();
            if(launchmps)
            unreal.drawImage(Images.get("Multiplayer Server"),0,0,resX+5,resY,this);
            else
            unreal.drawImage(Images.get("Multiplayer Client"),0,0,resX+5,resY,this);
            
            unreal.setColor(Color.black);
            unreal.drawString("UDP:"+socketnum,558,480);
            
            real.drawImage(offscreenM, 0, 0, resX+5, resY, this);
    
            if(!launchmp)
                break;
        }
        
    	return 0;
    	}
    
    private boolean saved_games_page(){
        Graphics real,unreal;
        String tstring;
        level_pick_color_inc_b=false;
        level_next_click=false;
        File saved_dir=new File("Saved Games"); //(localgamepath+"\\Saved Games");//might need this, if not, them don't need localgamepath
        File [] saved_games=saved_dir.listFiles();  // can throw null pointer expection--> need to fix!!!
        savedgamenum=saved_games.length;
        while(true){
            real = screen.getGraphics();
            //offscreenM = this.createImage(resX, resY);
            unreal = offscreenM.getGraphics();
            unreal.drawImage(Images.get("levelpick"),0,0,resX+5,resY,this);
            unreal.setColor(Color.black);
            if(level_next_click){
                file_to_be_loaded=saved_games[levelpickI-1];
                return true;
            }
            if(!show_save_b)
                return false;
            if(level_pick_color_inc_b){
                level_pick_color_I++;
                level_pick_color_inc_b=false;
                if(level_pick_color_I==level_pick_colors.length-1)
                    level_pick_color_I=0;
            }
            unreal.setColor(level_pick_colors[level_pick_color_I]);
            unreal.fillRect(558,515,615-558,555-542);
            if(levelpickI!=0){
                unreal.setColor(Color.blue);
                unreal.fillRect(274,414+20*(levelpickI-1),240,15);
            }
            unreal.setColor(Color.black);
            for(int d=0;d<savedgamenum;d++){
                //unreal.drawString(saved_games[d],415,224+20*d);//old
                unreal.drawString(saved_games[d].getName(),274,425+20*d);
            }
            
            real.drawImage(offscreenM, 0, 0, resX+5, resY, this);
        }
    }
    
    private void option_page(){
        Graphics real,unreal;
        String tstring;
        while(true){
            real = screen.getGraphics();
            //offscreenM = this.createImage(resX, resY);
            unreal = offscreenM.getGraphics();
            unreal.drawImage(Images.get("Options"),0,0,resX+5,resY,this);
            unreal.setColor(Color.black);
            unreal.drawString("Your System Resolution is \""+resX+"\" x \""+resY+"\"",415,224);
            unreal.drawString("For best preformance, 1024 x 768 is recommended.",415,244);
            if(attack_ani_b)
                tstring="----ON----";
            else
                tstring="----OFF----";
            unreal.drawString("Attack Animation    "+tstring,415,264);
            if(debug)
                tstring="----ON----";
            else
                tstring="----OFF----";
            unreal.drawString("Debugging to text is"+tstring,415,284);
            
            if(settings[2].equals("0"))
            	tstring="----ON----";
             else
             	tstring="----OFF----";
            unreal.drawString("In Game Music is:   "+tstring,415,304);
            
             if(menuclip.isRunning())
            	tstring="----ON----";
             else
             	tstring="----OFF----";
            unreal.drawString("Menu Music is:       "+tstring,415,324);
            
            real.drawImage(offscreenM, 0, 0, resX+5, resY, this);
            if(!showop)
                break;
        }
    }
    
    private int picklevel() {  // single player page
        Graphics real,or;
        Color player1=level_pick_colors[0];
        Image tem=Images.get("levelpick");
        levelpickb=true;
        level_next_click=false;
        levelpickI=0;
        
        while(true){
            real = screen.getGraphics();
            offscreen=this.createImage(resX,resY);
            or=offscreen.getGraphics();
            if(level_next_click)
                return levelpickI;
            if(!levelpickb)
                return 0;
                if(levelPlayerSideInc_b){
                currentPlayerInc_i++;
                if(currentPlayerInc_i==maxPlayers+1)
                currentPlayerInc_i=1;
                
                levelPlayerSideInc_b=false;
                }
            if(level_pick_color_inc_b){
                level_pick_color_I++;
                level_pick_color_inc_b=false;
                if(level_pick_color_I==level_pick_colors.length-1)
                    level_pick_color_I=0;
            }
            or.drawImage(tem,0,0,resX,resY,this);
            
            or.setColor(level_pick_colors[level_pick_color_I]);
            or.fillRect(558,515,615-558,555-542);
            if(levelpickI!=0){
                or.setColor(Color.blue);
                or.fillRect(274,414+20*(levelpickI-1),240,15);
            }
            or.setColor(Color.black);
            or.drawString("Player "+currentPlayerInc_i,558,480);
            for(int temp1=0;temp1<Levels.size();temp1++){
                or.drawString((String)Levels.get(temp1),274,425+20*temp1);
            }
            real.drawImage(offscreen,0,0,resX+5,resY,this);
        }
    }
    
    //not all that important or good
    private void show_Credits() {
        int totalcount=-900,credc=resY,delay=0;
        Graphics g = offscreenM.getGraphics();
        Graphics s = screen.getGraphics();
        while(true){
            
            g.setColor(Color.black);
            g.fillRect(0,0,resX,resY);
            g.setColor(Color.white);
            g.setFont( new Font("Arial",Font.BOLD,35));
            g.drawString("Aaron Newsom's",resX/3+10,credc);
            g.setFont( new Font("Arial",Font.BOLD,55));
            g.drawString("Window Wars",resX/3,credc+100);
            
            g.setFont( new Font("Arial",Font.CENTER_BASELINE,40));
            g.drawString("Image Design",resX/3,credc+200);
            g.setFont( new Font("Arial",Font.PLAIN,35));
            g.drawString("Stephen Killingsworth",resX/3,credc+250);
            
            g.setFont( new Font("Arial",Font.CENTER_BASELINE,40));
            g.drawString("Assistant Design",resX/3,credc+325);
            g.setFont( new Font("Arial",Font.PLAIN,35));
            g.drawString("Luke Sides",resX/3,credc+375);
            
            g.setFont( new Font("Arial",Font.CENTER_BASELINE,35));
            g.drawString("Testers",resX/3,credc+450);
            g.setFont( new Font("Arial",Font.PLAIN,30));
            g.drawString("Luke Sides",resX/3,credc+500);
            g.drawString("Julien Gradnigo",resX/3,credc+550);
            g.drawString("Tyler Guthry",resX/3,credc+600);
            g.drawString("Stephen Killingsworth",resX/3,credc+650);
            
            g.setFont( new Font("Arial",Font.CENTER_BASELINE,40));
            g.drawString("Special Thanks",resX/3,credc+750);
            g.setFont( new Font("Arial",Font.PLAIN,35));
            g.drawString("Mark Broussard",resX/3,credc+800);
            g.drawString("George Lucas",resX/3,credc+850);
            
            if(credc<=-850){
                g.drawImage(Images.get("Credits"), resX/3, resY/3, resX/3, resY/3, this);
                delay=100;
            }
            
            credc--;
            s.drawImage(offscreenM, 0, 0, resX+5, resY, this);
            try{Thread.currentThread().sleep(delay); //load delay
            } catch(InterruptedException er1){new ErrorGen(this,"ThreadError",er1);}
            if(credc==totalcount||!showcr) {
                showcr=false;
                credc=0;
                break;
            }
        }
    }
    
    private ArrayList level(File loadFile)throws NumberFormatException {
    	
    	ObjectInputStream fileIn;
    	Object ojb;
    	try{
    		fileIn=new ObjectInputStream(new FileInputStream(loadFile));
    		ojb=fileIn.readObject();
    		if(ojb.getClass().getName().equals("JavaGameP.Gamemap"))
    		g=(Gamemap)ojb;
    		else
    		debugger.println(ojb.getClass().getName());
    	}catch(IOException IOE){new ErrorGen(this,"IOE",IOE);}
    	catch(ClassNotFoundException CNFE){new ErrorGen(this, "CNFE",CNFE);}
    	

        levelstart(1,"Saved Game");//needs something else
        return null;//savedobj;
    }
    
    private ArrayList level(Gamemap gm, int num) {
        ArrayList savedobj = new ArrayList();
        switch (num) {
            case 1:
                savedobj.add(new Base(g, new Zone(500, 200, 100, 100), g.players[1]));
                savedobj.add(new SpecialOps(g, new Zone(300, 150, 100, 100), g.players[1]));
                savedobj.add(new Barracks(g, new Zone(700, 100, 100, 100), g.players[1]));
                savedobj.add(new Barracks(g, new Zone(200, 500, 100, 100), g.players[2]));
                savedobj.add(new Marine(g, new Zone(780, 600, 40, 40), g.players[2]));
                savedobj.add(new Marine(g, new Zone(700, 600, 40, 40), g.players[2]));
                savedobj.add(new Sniper(g, new Zone(200, 200, 40, 40), g.players[1]));
                savedobj.add(new Mineral(g, new Zone(20, 20, 35, 35)));
                savedobj.add(new Mineral(g, new Zone(1000, 1000, 35, 35)));
                savedobj.add(new Base(g, new Zone(1300, 1000, 100, 100), g.players[2]));
                savedobj.add(new Sniper(g, new Zone(1300, 900, 40, 40), g.players[2]));
                savedobj.add(new Marine(g, new Zone(1200, 1000, 40, 40), g.players[2]));
                savedobj.add(new Sniper(g, new Zone(1200, 900, 40, 40), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(20, 250, 110, 50), g.players[1]));
                savedobj.add(new Paladin(g, new Zone(900, 800, 110, 50), g.players[2]));
                savedobj.add(new Jedi(g, new Zone(100, 500, 31, 38), g.players[1]));
                
                vx=0; // sets starting veiwpoint
                vy=0;
                gm.setfieldX(1500); // sets size of total battlefield
                gm.setfieldY(1200);
                try { try{ try{
                    File tempfile = new File("B1.wav");
                    AudioInputStream audioSource = AudioSystem.getAudioInputStream(tempfile);
                    DataLine.Info info = new DataLine.Info(Clip.class, audioSource.getFormat());
                    backgroundclip = (Clip)AudioSystem.getLine(info);
                    backgroundclip.open(audioSource);
                }
                catch (UnsupportedAudioFileException e) {
                    new ErrorGen(this,"Sound-UnsupportedAudioFileException",e);
                }}catch(LineUnavailableException errorl){new ErrorGen(this,"Sound-LineUnavailableException",errorl);}
                }catch(IOException errorIO){new ErrorGen(this,"Sound-IOException",errorIO);}
                
                backgroundMusicFileName="B1.wav";
                levelbackgroundname="grass";
                gm.players[1].setminerals(500);
                gm.players[2].setminerals(1000);
                levelstart(1,"Destroy the enemy Command Center");
                return savedobj;
            case 2:
                savedobj.add(new Base(g, new Zone(300, 200, 100, 100), g.players[1]));
                savedobj.add(new Drone(g, new Zone(85, 85, 40, 40), g.players[1]));
                savedobj.add(new Drone(g, new Zone(105, 85, 40, 40), g.players[1]));
                savedobj.add(new Mineral(g, new Zone(20, 20, 35, 35)));
                savedobj.add(new Mineral(g, new Zone(50, 20, 35, 35)));
                savedobj.add(new Mineral(g, new Zone(50, 100, 35, 35)));
                savedobj.add(new Mineral(g, new Zone(100, 20, 35, 35)));
                savedobj.add(new SpecialOps(g, new Zone(1300, 1000, 100, 100), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(900, 800, 110, 50), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(800, 1000, 110, 50), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(1100, 800, 110, 50), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(1000, 1000, 110, 50), g.players[2]));
                vx=0; // sets starting viewpoint
                vy=0;
        		gm.setfieldX(1500);
                gm.setfieldY(1200);
                try { try{ try{
                    File tempfile = new File("B1.wav");
                    AudioInputStream audioSource = AudioSystem.getAudioInputStream(tempfile);
                    DataLine.Info info = new DataLine.Info(Clip.class, audioSource.getFormat());
                    backgroundclip = (Clip)AudioSystem.getLine(info);
                    backgroundclip.open(audioSource);
                }
                catch (UnsupportedAudioFileException e) {
                    new ErrorGen(this,"Sound-UnsupportedAudioFileException",e);
                }}catch(LineUnavailableException errorl){new ErrorGen(this,"Sound-LineUnavailableException",errorl);}
                }catch(IOException errorIO){new ErrorGen(this,"Sound-IOException",errorIO);}
                
                backgroundMusicFileName="B1.wav";
                levelbackgroundname="dirt";
                gm.players[1].setminerals(1000);
                gm.players[2].setminerals(1000);
                levelstart(2,"Destroy the enemy Special Ops");
                return savedobj;
            case 3:
                savedobj.add(new Base(g, new Zone(100, 100, 0, 0), g.players[1]));
                savedobj.add(new Mineral(g, new Zone(10, 50, 0, 0)));
                savedobj.add(new Mineral(g, new Zone(10, 20, 0, 0)));
                savedobj.add(new Mineral(g, new Zone(10, 80, 0, 0)));
                savedobj.add(new Mineral(g, new Zone(40, 20, 0, 0)));
                savedobj.add(new Mineral(g, new Zone(70, 20, 0, 0)));
                savedobj.add(new Mineral(g, new Zone(10, 110, 0, 0)));
                savedobj.add(new Mineral(g, new Zone(100, 20, 0, 0)));
                savedobj.add(new Drone(g, new Zone(50, 50, 0, 0), g.players[1]));
                savedobj.add(new Base(g, new Zone(1300, 1000, 0, 0), g.players[2]));
                savedobj.add(new Sniper(g, new Zone(1300, 900, 40, 40), g.players[2]));
                savedobj.add(new Sniper(g, new Zone(1200, 1000, 40, 40), g.players[2]));
                savedobj.add(new Sniper(g, new Zone(1200, 900, 40, 40), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(20, 250, 110, 50), g.players[1]));
                savedobj.add(new Paladin(g, new Zone(900, 800, 110, 50), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(1000, 1000, 110, 50), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(1300, 700, 110, 50), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(700, 800, 110, 50), g.players[2]));
                savedobj.add(new Jedi(g, new Zone(50, 200, 31, 38), g.players[1]));
                vx=0; // sets starting veiwpoint
                vy=0;
                gm.setfieldX(1500);
                gm.setfieldY(1200);
                try { try{ try{
                    File tempfile = new File("B2.wav");
                    AudioInputStream audioSource = AudioSystem.getAudioInputStream(tempfile);
                    DataLine.Info info = new DataLine.Info(Clip.class, audioSource.getFormat());
                    backgroundclip = (Clip)AudioSystem.getLine(info);
                    backgroundclip.open(audioSource);
                }
                catch (UnsupportedAudioFileException e) {
                    new ErrorGen(this,"Sound-UnsupportedAudioFileException",e);
                }}catch(LineUnavailableException errorl){new ErrorGen(this,"Sound-LineUnavailableException",errorl);}
                }catch(IOException errorIO){new ErrorGen(this,"Sound-IOException",errorIO);}
                
                backgroundMusicFileName="B2.wav";
                levelbackgroundname="grass";
                gm.players[1].setminerals(100);
                gm.players[2].setminerals(1000);
                levelstart(3,"Destroy the Enemy");
                return savedobj;
        	case 4:
                savedobj.add(new Base(g, new Zone(100, 100, 0, 0), g.players[1]));
                savedobj.add(new Mineral(g, new Zone(10, 50, 0, 0)));
                savedobj.add(new Mineral(g, new Zone(10, 20, 0, 0)));
                savedobj.add(new Mineral(g, new Zone(10, 80, 0, 0)));
                savedobj.add(new Mineral(g, new Zone(40, 20, 0, 0)));
                savedobj.add(new Mineral(g, new Zone(70, 20, 0, 0)));
                savedobj.add(new Mineral(g, new Zone(10, 110, 0, 0)));
                savedobj.add(new Mineral(g, new Zone(100, 20, 0, 0)));
                savedobj.add(new Drone(g, new Zone(50, 50, 0, 0), g.players[1]));
                savedobj.add(new Base(g, new Zone(2250, 100, 0, 0), g.players[2]));
                savedobj.add(new Sniper(g, new Zone(1700, 100, 40, 40), g.players[2]));
                savedobj.add(new Sniper(g, new Zone(2400, 100, 40, 40), g.players[2]));
                savedobj.add(new Sniper(g, new Zone(2100, 300, 40, 40), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(20, 250, 110, 50), g.players[1]));
                savedobj.add(new Paladin(g, new Zone(2000, 100, 110, 50), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(1900, 600, 110, 50), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(1500, 700, 110, 50), g.players[2]));
                savedobj.add(new Paladin(g, new Zone(990, 400, 110, 50), g.players[2]));
                savedobj.add(new Jedi(g, new Zone(50, 200, 31, 38), g.players[1]));
                vx=0; // sets starting veiwpoint
                vy=0;
                gm.setfieldX(2500);
                gm.setfieldY(800);
                try { try{ try{
                    File tempfile = new File("B2.wav");
                    AudioInputStream audioSource = AudioSystem.getAudioInputStream(tempfile);
                    DataLine.Info info = new DataLine.Info(Clip.class, audioSource.getFormat());
                    backgroundclip = (Clip)AudioSystem.getLine(info);
                    backgroundclip.open(audioSource);
                }
                catch (UnsupportedAudioFileException e) {
                    new ErrorGen(this,"Sound-UnsupportedAudioFileException",e);
                }}catch(LineUnavailableException errorl){new ErrorGen(this,"Sound-LineUnavailableException",errorl);}
                }catch(IOException errorIO){new ErrorGen(this,"Sound-IOException",errorIO);}
                
                backgroundMusicFileName="B2.wav";
                levelbackgroundname="grass";
                gm.players[1].setminerals(100);
                gm.players[2].setminerals(1000);
                levelstart(3,"Destroy the Enemy");
                return savedobj;               
            default:
                return null;
        }
        
        
    }
    private void draw_background() {
        Graphics g = offscreenM.getGraphics();
        g.drawImage(backgroundM, 0, 0, resX+5, resY, this);
    }
    
    private void run() throws InterruptedException, CloneNotSupportedException {
        //debugger.println("run()");//Debug
        gamerun=true;
        offscreen = createImage(g.getfieldX(), g.getfieldY()); //offscreen, all other draw methods draw here
        while (true) {
            if(forcepause)
                loop();
            Graphics gg = screen.getGraphics();
            //debugger.println(gamecount);//Debugger
            checkscreenmovement();
            g.step(p, mp, mdown);
            if(exitCurrentGame)
            break;
            gamecount++;
            Thread.sleep(10);//delay********* once game is great this will make the game run at the same speed on all computers
            System.gc();
            // debugger.println ("end of run()"); //Debug
            gg.drawImage(offscreen, 0, 0, resX+5, resY, vx, vy, vx + resX, vy + resY, this); // Draw buffered Image
        }
    }
    
    private void loop(){
        //backgroundclip.stop();
        while(true){
            if (selunpause){
                forcepause=false;
                break;
            }
            if(time_to_save){
                g.save_game();
                time_to_save=false;
            }
            show_pause_menu();
        }
        selunpause=false;
    }
    private void show_pause_menu(){
        Graphics gg = screen.getGraphics();
        Graphics og=offscreen.getGraphics();
        og.drawImage(Images.get("PauseMenu"),vx+resX/2-100,vy+resY/2-200,200,400,this);
        gg.drawImage(offscreen, 0, 0, resX+5, resY, vx, vy, vx + resX, vy + resY, this); // Draw buffered Image
    }
    
    public void showselrect(Point startdrag, Point movenow) {
        Graphics g = offscreen.getGraphics();
        g.setColor(Color.green);
        if(movenow.x - startdrag.x>0)
            g.drawRect(startdrag.x, startdrag.y, movenow.x - startdrag.x, movenow.y - startdrag.y);
        else
            g.drawRect(startdrag.x, startdrag.y,  startdrag.x-movenow.x,  startdrag.y-movenow.y);
    }
    
    
    private void checkscreenmovement() {
        if (mpr.x > resX - 20 && vx <= g.getfieldX() - resX+125)//new
            movescreen(2);
        else if (mpr.x < 20 && vx >= 10)
            movescreen(4);
        if (mpr.y > resY - 20 && vy <= g.getfieldY() - resY)
            movescreen(3);
        else if (mpr.y < 40 && vy >= 10)
            movescreen(1);
    }
    
    private void gamestart() {
        Graphics sg=screen.getGraphics();
        offscreen=createImage(resX,resY);
        Graphics gg=offscreen.getGraphics();
        gg.drawImage(Images.get("SWG"),0, 0, resX, resY,this);
        gg.setColor(Color.white);
        Font tempfont=new Font("Arial",Font.BOLD,25);
        gg.setFont(tempfont);
        gg.drawString("Loading.....", resX/2-60, resX/2);
        gg.drawRect(resX/4,3*resY/4,resX/2,50);
        for (int d = 0 ; d < resX/2 ; d++) {
            gg.fillRect(resX/4,3*resY/4,d,50);
            sg.drawImage(offscreen,0,0,resX,resY,this);
        }
    }
    
    private void levelstart(int  levelnum,String loadtext) {
        Graphics sg=screen.getGraphics();
        offscreen=createImage(resX,resY);
        Graphics gg=offscreen.getGraphics();
        gg.drawImage(Images.get("SWG"),0, 0, resX, resY,this);
        gg.setColor(Color.white);
        Font tempfont=new Font("Arial",Font.BOLD,25);
        gg.setFont(tempfont);
        gg.drawString("Loading.....", resX/2-60, resX/2);
        gg.drawString("Mission "+levelnum+ ":", 250, 200);
        gg.drawString(loadtext, 250, 250);
        gg.drawRect(resX/4,3*resY/4,resX/2,50);
        for (int d = 0 ; d < resX/2 ; d+=2) {
            gg.fillRect(resX/4,3*resY/4,d,50);
            sg.drawImage(offscreen,0,0,resX,resY,this);
        }
    }
    
    /*
     *	Specifics what mouse clicks do when clicking things in the options page (from the main menu)
     */
    private void options_M (int x, int y)
    {
   	if (x > 975 && y > 117 && x < 991 && y < 131)//exit
   		showop=false;
    else if (x > 521 && y > 279 && x < 572 && y < 294){ //ani change
         if(attack_ani_b)
        	 attack_ani_b=false;
         else
             attack_ani_b=true;
         }
    else if (x > 521 && y > 299 && x < 572 && y < 314){ //debug change
         if(debug)
             debug=false;
         else
             debug=true;
         }
    else if (x > 521 && y > 319 && x < 572 && y < 334){ //menu music change ***in game music control is missing
         if(settings[2].equals("0"))
               settings[2]=1;
         else
             settings[2]=0;
         }
    else if (x > 521 && y > 339 && x < 572 && y < 354){ //menu music change ***in game music control is missing
         if(menuclip.isRunning()){
               menuclip.stop();
               settings[0]=1;
         	   }
         else{
             menuclip.loop(menuclip.LOOP_CONTINUOUSLY);
             settings[0]=0;
             }
         }
    }
    
    /*
     *	Specifies what mouse clicks do when clicking things in the Single Player page (also called the level pick page)
     */
    private void level_pick_M (int x, int y)
    {
        for(int temp1=0;temp1<Levels.size();temp1++){
             if (x > 275 && y > 444+temp1*20 && x < 515 && y < 454+temp1*20)
                 levelpickI=temp1+1;
         }
         if(x > 270 && y > 560 && x < 405 && y < 581)
              level_next_click=true;
         else if(x > 498 && y > 600 && x < 559 && y < 618)
             levelpickb=false;
         else if(x > 558 && y > 500 && x < 624 && y < 520)
             levelPlayerSideInc_b=true;
         else if(x > 558 && y > 540 && x < 624 && y < 555)
                    level_pick_color_inc_b=true;   	
    }
    
    private void ingamePauseMenu_M(int x,int y){
	    if(x>431&&x<600&&y>285&&y<325)
	        selunpause=true;
	    if(x>431&&x<600&&y>327&&y<367)
	        time_to_save=true;
	    if(x>431&&x<600&&y>433&&y<474){
	        if(backgroundclip.isRunning())
	            backgroundclip.stop();
	        else
	            backgroundclip.loop(backgroundclip.LOOP_CONTINUOUSLY);
	    }
	    if(x>431&&x<600&&y>484&&y<524){
	        exitCurrentGame=true;
	        selunpause=true;
	    }
    }
    
    private void savedGamePage_M(int x,int y){
                for(int temp1=0;temp1<savedgamenum;temp1++){
                    if (x > 275 && y > 444+temp1*20 && x < 515 && y < 454+temp1*20)
                        levelpickI=temp1+1;
                }
                if(x > 270 && y > 560 && x < 405 && y < 581)
                    level_next_click=true;
                if(x > 498 && y > 600 && x < 559 && y < 618)
                    show_save_b=false;
                if(x > 558 && y > 540 && x < 624 && y < 555)
                    level_pick_color_inc_b=true;
    }
    
    public boolean mouseDown(Event Mousem, int x, int yy)  // Event if mouse button pressed
    {
    	int y=yy+27;//just for balance issues- the taskbar looking thing ingame
        if(debug)
        debugger.println("x: " + x + "  y: " + y); //Debug Info
        
        if(menub){
            if(levelpickb)//if the level pick page is up (single player)
				level_pick_M(x,y);
            else if(launchmp){
            	
            	if(launchmps){ // if on server mode
            		if(x > 254 && y > 212 && x < 306 && y < 228)
            		launchmps=false;
            	}
            	else if(x > 310 && y > 212 && x < 356 && y < 228)
            	launchmps=true;
            	
            	if(x > 498 && y > 600 && x < 559 && y < 618) //will work in both client and server mode
                    launchmp=false;
            }
            
            else if(showcr)
                return true;
            else if (showop) //if options page is up
                options_M(x,y);
            else if(show_save_b)
            	savedGamePage_M(x,y);
            // single player launch!!!
            else if (x > 7 && y > 395 && x < 168 && y < 448){
                launchsp = true;
            }// need to make --> loadsp = true  to launch loadlevel menu right here!!!!!!!
            //Multiplayer launch
            else if (x > 7 && y > 450 && x < 168 && y < 500){
                launchmp = true;
            }
            else if(x > 324 && y > 720 && x < 480 && y < 755) // if "turn off computer is clicked"
           		exit();
            else if(x > 9 && y>512 && x<180 &&y<555)//9,512  180,555
                showcr=true;
            else if(x>255&&y>566&&x<412&&y<610)
                showop=true;
            else if(x>250&&y>450&&x<435&&y<500)
                show_save_b=true;
            return true;
        }
        else if(forcepause)
        	ingamePauseMenu_M(x,y);
        else if(gamerun){
            newmclick=true;
            mdown = true;
            //int z = Mousem.getModifiers ();//need InputEvent, but that won't work here
            p = new Point(x + vx, y - 20 + vy);
            //debugger.println(x+" "+y); // Debug
            return true;
        }
        return true;
    }
    
    
    public boolean mouseUp(Event Mousem, int x, int y) {
        if(menub)
            return true;
        else if(gamerun){
            mdown = false;
            return true;
        }
        return true;
    }
    
    
    public boolean mouseDrag(Event Mousem, int x, int y) {
        if(menub)
            return true;
        else if(gamerun){
            mp = new Point(x + vx, y - 20 + vy);
            return true;
        }
        return true;
    }
    
    
    public boolean mouseMove(Event Mousem, int x, int y) {
        if(menub)
            return true;
        else if(gamerun){
            mp = new Point(x + vx, y - 20 + vy);
            mpr = new Point(x, y);
            return true;
        }
        return true;
    }
    
    
    public void movescreen(int dir)  //1=up, 2=right, 3=down, 4=left
    {
        if (dir == 1)
            vy -= 10;
        else if (dir == 2)
            vx += 10;
        else if (dir == 3)
            vy += 10;
        else if (dir == 4)
            vx -= 10;
    }
    
    
    public boolean keyDown(Event Key1, int keynum) {
        if(menub){
            if(showcr){
                showcr=false;
            }
            return true;
        }
        else if (keynum==112){ // p pause/unpause
            if(!forcepause)
                forcepause=true;
            else if(forcepause)
                selunpause=true;
        }
        else if(gamerun&& !forcepause){
            if(g.getDebug())debugger.println("key " + keynum); //Debug
            if(keynum==126)// ~ -acticates Debugging mode
            g.turnDebug();
            else if (keynum == 1004 && vy >= 10) // up
                movescreen(1);
            else if (keynum == 1005 && vy <= g.getfieldY() - resY) // down
                movescreen(3);
            else if (keynum == 1006 && vx >= 10) // left
                movescreen(4);
            else if (keynum == 1007 && vx <= g.getfieldX() - resX+125) // right
                movescreen(2);
            else if (keynum == 104) // h    home veiw (vx=0,vy=0)
                setveiw(0,0);
            else if (keynum == 100) // d    deselect unit(s)
                g.deselect();
            else if (keynum == 115) //s    stop selected unit(s)
                g.stopsel();
            else if (keynum == 127) // Delete    kills selected unit(s)
                g.killsel();
            else if (g.selobjsave(keynum)) //save group slots
                ;
            // if (keynum == 27) //quit 'x'
            //autodefeat=true;
            else if (keynum == 97) // 'a' --sets unit stance to "attack-move"
                g.set_sel_attackmove(true);
            else if (keynum == 108) // 'l' --sets unit stance to 'move'
                g.set_sel_attackmove(false);
            else if (keynum==117) // 'u' --autoreturns drones with mineral
                g.returndroneswithminerals();
            return true;
        }
        return true;
    }
    
    public void setveiw(int newx,int newy){
        vx=newx;
        vy=newy;
    }
    
    public void set_building_location(String building_name,boolean start){
        if(!start){
            building_ghost_b=false;
            building_ghost=null;
            return;
        }
        building_ghost_b=true;
        building_ghost=Images.get(building_name);//needs '+ghost'
    }
    
    /**
     *	displayObj- called by Gamemap.step() to draw everthing on the screen
     */
    public void displayObj(ArrayList objs, ArrayList selectedobj,ArrayList<Integer>next_click_building_progresses) {
        Graphics rg = offscreen.getGraphics();
        GameObject currentObj;
        IZone current_ghost_z;
        ArrayList ghost_ImageZones=new ArrayList(1);
        for (int d = 0 ; d < objs.size() ; d++) //draw all objects
        {
            currentObj=(GameObject) objs.get(d);
            currentz = currentObj.getImageZone();
            currenti = currentz.getImage();
            rg.drawImage(currenti, currentz.x, currentz.y, currentz.width, currentz.height, null);
            rg.setColor(currentObj.getowner().getColor());
            rg.drawLine(currentz.x, currentz.y, (int) (currentz.x + (double) currentObj.gethitpoints() / (double) currentObj.getfullhitpoints() * (double) currentz.width), currentz.y); //draw healthbar
           // rg.drawImage((Image)Images.get("Flames"),currentz.x,currentz.y,currentz.width,currentz.height,null);//attempt to draw damage, needs better pic
            if(currentObj.builder){
                current_ghost_z= ((ProductionUnit)currentObj).check_for_ghosts_and_set_uplink();
                if(current_ghost_z!=null)
                    ghost_ImageZones.add(current_ghost_z);
            }
        }
        if (selectedobj != null && selectedobj.size() != 0) {
            for (int selobjcount = 0 ; selobjcount < selectedobj.size() ; selobjcount++) //multiple selobj
            {
                Zone z = ((GameObject) selectedobj.get(selobjcount)).getZone();
                rg.setColor(Color.green);
                rg.drawOval(z.x - 5, z.y - 5, z.width + 10, z.height + 10);
            }
        }
        if(ghost_ImageZones.size()!=0){
            for(int temp1=0;temp1<ghost_ImageZones.size();temp1++){
                current_ghost_z=(IZone)ghost_ImageZones.get(temp1);
                rg.drawImage(current_ghost_z.getImage(),current_ghost_z.x,current_ghost_z.y,current_ghost_z.width,current_ghost_z.height,this);
                if(g.isEmpty(current_ghost_z.getZone()))
                    rg.setColor(Color.green);
                else
                    rg.setColor(Color.red);
                rg.drawLine(current_ghost_z.x,current_ghost_z.y,current_ghost_z.x+current_ghost_z.width,+current_ghost_z.y+current_ghost_z.height);
                rg.drawLine(current_ghost_z.x+current_ghost_z.width,current_ghost_z.y,current_ghost_z.x,current_ghost_z.y+current_ghost_z.height);
                rg.drawLine(current_ghost_z.x+current_ghost_z.width/2,(int)(current_ghost_z.y+current_ghost_z.height-(double)next_click_building_progresses.get(temp1)*(double)current_ghost_z.height/100.0),current_ghost_z.x+current_ghost_z.width/2,current_ghost_z.y+current_ghost_z.height);
            }
            ghost_ImageZones.clear();
            next_click_building_progresses.clear();
        }
        if(building_ghost_b){
            rg.drawImage(building_ghost, mp.x, mp.y, 100, 100, this);//needs width/height fixers
            if(g.isEmpty(new Zone(mp.x, mp.y, 100, 100)))
                rg.setColor(Color.green);
            else
                rg.setColor(Color.red);
            rg.drawRect(mp.x, mp.y, 100, 100);
        }
        Graphics a = offscreen.getGraphics();
        a.setColor(Color.gray);
        a.fillRect(resX - 150 + vx, vy, 150, resY);
        if (selectedobj != null && selectedobj.size() != 0&&!g.get_next_click_building()) {
            if(((GameObject) selectedobj.get(0)).getFactory()){
                GameObject pb;
                if(!((GameObject) selectedobj.get(0)).getBuilder()) {
                    pb = (GameObject) selectedobj.get(0);
                    buildingtab(((ProductionBuilding)pb).getProductionItems(), ((ProductionBuilding)pb).getque(), (ProductionBuilding)pb);
                }
                else{
                    pb = (GameObject) selectedobj.get(0);
                    buildingtab(((ProductionUnit)pb).getProductionItems(), ((ProductionUnit)pb).getque(), (ProductionUnit)pb);
                }
            }
            else{
                unittab((GameObject) selectedobj.get(0));
            }
        }
        showwplayerinfo();
    }
    
    
    private void showwplayerinfo() {
        Graphics a = offscreen.getGraphics();
        a.setColor(Color.gray);
        a.fillRect(vx, vy, 35, 10);
        a.setColor(Color.white);
        a.drawString(Integer.toString(g.currentPlayer.getminerals()), 5 + vx, 9 + vy);
    }
    
    
    public void showbackground() {
        Graphics og = offscreen.getGraphics();
        og.drawImage(Images.get("background "+g.levelbackgroundname), 0, 0, g.getfieldX(), g.getfieldY(), this);
    }
    
    private void unittab(GameObject selectedobj){
        Graphics a = offscreen.getGraphics();
        ArrayList<String> unit_ablities;
        int decy = 100;
        Image i=selectedobj.getImageZone().getImage();
        a.drawImage(i, 900 + vx, 20 + vy, 60, 60, this);
        a.setColor(Color.white);
        a.drawLine(900 + vx, 20 + vy, (int) (resX-120+ 50 * ((double) selectedobj.gethitpoints() / (double) selectedobj.getfullhitpoints()) + vx), 20 + vy);
        unit_ablities= AbilityList.get(selectedobj.getName());
        if(unit_ablities==null)
            return;
        for (int d = 0 ; d < unit_ablities.size() ; d++) {
            a.drawString(unit_ablities.get(d), 900 + vx, decy + 60 + vy);
            decy += 75;
        }
    }
    
    private void buildingtab(ArrayList<String> buildingobjs, LinkedList quelist, ProductionUnit selectedobj) {
        Image i;
        GameObject temp1;
        int decy = 20;
        Resource tempr;
        Graphics a = offscreen.getGraphics();
        a.setColor(Color.white);
        for (int d = 0 ; d < buildingobjs.size() ; d++) {
            i = Images.get(buildingobjs.get(d));
            a.drawImage(i, 900 + vx, decy + vy, 60, 60, this);
            a.drawString(buildingobjs.get(d), 900 + vx, decy + 60 + vy);
            tempr= Costs.get(buildingobjs.get(d));
            a.drawString(tempr.res1 + "    |    " + tempr.res2, 900 + vx, decy + 70 + vy);
            decy += 75;
            if (decy > resY)
                break;
        }
        decy = resY - 100;
        for (int d = 0 ; d < quelist.size() ; d++) {
            i = ((GameObject) quelist.get(d)).getImageZone().getImage();
            a.drawImage(i, 900 + vx, decy + vy, 60, 60, this);
            decy -= 75;
            if (decy > resY)
                break;
        }
        if (quelist.size() != 0)
            a.drawLine(900 + vx, resY - 110 + vy, (int) (950 - 50 * ((double) selectedobj.getcurrentcount() / (double) ((GameObject) quelist.get(0)).getbuildspeed())) + vx, resY - 110 + vy);
    }
    
    private void buildingtab(ArrayList buildingobjs, LinkedList quelist, ProductionBuilding selectedobj) {
        Image i;
        GameObject temp1;
        Resource tempr;
        int decy = 20;
        Graphics a = offscreen.getGraphics();
        //        a.setColor(Color.gray);
        //        a.fillRect(resX - 150 + vx, vy, 150, resY);
        a.setColor(Color.white);
        for (int d = 0 ; d < buildingobjs.size() ; d++) {
            //temp1=ClassLoad.load_string((String) buildingobjs.get(d),g,g.players[0]);
            i =Images.get((String) buildingobjs.get(d));//temp1.getImageZone().getImage();
            a.drawImage(i, 900 + vx, decy + vy, 60, 60, this);
            a.drawString((String)buildingobjs.get(d), 900 + vx, decy + 60 + vy);
            tempr= (Resource)Costs.get(buildingobjs.get(d));
            if(tempr!=null)
            a.drawString(tempr.res1 + "    |    " + tempr.res2, 900 + vx, decy + 70 + vy);
            decy += 75;
            if (decy > resY)
                break;
        }
        decy = resY - 100;
        for (int d = 0 ; d < quelist.size() ; d++) {
            i = ((GameObject) quelist.get(d)).getImageZone().getImage();
            a.drawImage(i, 900 + vx, decy + vy, 60, 60, this);
            decy -= 75;
        }
        if (quelist.size() != 0)
            a.drawLine(900 + vx, resY - 110 + vy, (int) (950 - 50 * ((double) selectedobj.getcurrentcount() / (double) ((GameObject) quelist.get(0)).getbuildspeed())) + vx, resY - 110 + vy);
    }
    
    
    public boolean showmove(Point m, int t) {
        type = t;
        movepoint = m;
        w = 2;
        return true;
    }
    
    
    public boolean showmove() {
        Graphics a = offscreen.getGraphics();
        if (type == 1)
            a.setColor(Color.green);
        else if (type == 2)
            a.setColor(Color.red);
        a.drawOval(movepoint.x - w / 2, movepoint.y - w / 2, w, w);
        if (w == 20)
            return false;
        w += 2;
        return true;
    }
    public void stopmusic(){
        if(backgroundclip!=null)
            backgroundclip.stop();
        if(menuclip!=null)
            menuclip.stop();
    }
    
    
    public void showVictory() {
        backgroundclip.stop();
        backgroundclip.flush();
        Graphics sg=screen.getGraphics();
        offscreen=createImage(resX,resY);
        Graphics gg=offscreen.getGraphics();
        gg.drawImage(Images.get("SWG"),0, 0, resX, resY,this);
        gg.setColor(Color.white);
        Font tempfont=new Font("Arial",Font.BOLD,30);
        gg.setFont(tempfont);
        gg.drawString("Victory!", resX/2-60, resX/2);
        sg.drawImage(offscreen,0,0,resX,resY,this);
        
        try {
            Thread.sleep(1000);
        }
        catch(InterruptedException e) {
            new ErrorGen(this,"ThreadError",e);
        }
        //GameControl();
 exitCurrentGame=true;
    }
    
    
    public void showDefeat() {
        autodefeat=false;
        backgroundclip.stop();
        backgroundclip.drain();
        Graphics sg=screen.getGraphics();
        offscreen=createImage(resX,resY);
        Graphics gg=offscreen.getGraphics();
        gg.drawImage(Images.get("SWG"),0, 0, resX, resY,this);
        gg.setColor(Color.white);
        Font tempfont=new Font("Arial",Font.BOLD,30);
        gg.setFont(tempfont);
        gg.drawString("Defeat!", resX/2-60, resX/2);
        sg.drawImage(offscreen,0,0,resX,resY,this);
        try {
            Thread.sleep(1000);
        }
        catch(InterruptedException e) {
            new ErrorGen(this,"ThreadError",e);
        }
        //GameControl();
        exitCurrentGame=true;
    }
    
    public Point res(){
        return new Point(resX,resY);
    }
    public int resX(){
    	return resX;
    }
    public int resY(){
    	return resY;
    }
    public Image getOffscreen(){
        return offscreen;
    }
    
    public boolean md() {
        return mdown;
    }
    
    
    
    public void attackAnimation(FightingUnit holder,GameObject target)  //work in progress
    {
        if(!attack_ani_b)
            return;
        Point tz = target.getCenter();
        Point hz=holder.getCenter();
        Graphics a = offscreen.getGraphics();
        a.setColor(Color.red);
        a.drawLine(hz.x+(tz.x-hz.x)/2+(tz.x-hz.x)/4,hz.y+(tz.y-hz.y)/2+(tz.y-hz.y)/4,hz.x+(tz.x-hz.x)/2-(tz.x-hz.x)/4,hz.y+(tz.y-hz.y)/2-(tz.y-hz.y)/4);
        a.drawOval(tz.x, tz.y, 5, 5);
    }
    
    /*
     *	Called When the game exits (only when exits normally- through main menu)
     */
    private void exit()
    {
    	if(menuclip.isRunning())
    		settings[0]=0;
    	else
    		settings[0]=1;
    	if(debug)
    		settings[1]=1;
    	else
    		settings[1]=0;
    		
    	//settings file write
    	PrintWriter settingsWriter=null;
    	try{settingsWriter= new PrintWriter(new File("Settings.ww"));
        }catch(Exception HELP){new ErrorGen(this,"Settings Writer",HELP);}
    	for(int d=0;d<10;d++)
    		settingsWriter.println(settings[d]);
    	settingsWriter.close();
    	debugger.close();
    	System.exit(0);
    }
    
    public static void main(String[] args) {
 			File onerun=new File("running.tmp");
 			if(onerun.exists())
 			System.exit(0);
 			try{
 			onerun.createNewFile() ;
 			onerun.deleteOnExit(); 	
 			}catch (Exception e){new ErrorGen(null,"Running Error "+e.toString()+"  ",e);}		
        	GameDisplay GD=null;
            try{
               GD=new GameDisplay();
            }catch (Exception e){new ErrorGen(GD,"Catch All 1"+e.toString()+"  ",e);}   
            try{
               GD.GameControl();
            }catch (Exception e){new ErrorGen(GD,"Catch All 2"+e.toString()+"  ",e);}
        //}
    } // main method
    
    //---------------------------------------------------------------------------------------
    // The following seven methods are defined in the
    // WindowListener interface.
    public void windowActivated(WindowEvent ee) {}
    
    public void windowClosing(WindowEvent ee) {
    	debugger.close();
        System.gc();
        System.exit(0); // Quit the program.
    }

    public void windowClosed(WindowEvent ee) {} // windowClosed method
    
    
    public void windowDeactivated(WindowEvent ee) {} // windowDeactivated method
    
    
    public void windowDeiconified(WindowEvent ee) {} // windowIconified method
    
    
    public void windowIconified(WindowEvent ee) {} // windowIconified method
    
    public void windowOpened(WindowEvent ee) {}
    //------------------------------------------------------------------------------------
} // GameDisplay class
