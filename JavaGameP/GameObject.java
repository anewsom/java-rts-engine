package JavaGameP;

import java.util.*;
import java.awt.*;
import java.io.Serializable;
/**
 *	GameObject
 *	The super object from which all game Objects subclass
 *	contains everything an object needs to be placed in the game
 *	for example, Future plans entail creating game landscape objects such as rocks or rivers that decend directly from GameObject
 */
public abstract class GameObject implements Cloneable, Serializable {
    final transient static int Foot=1,Armored=2,Building=3;
    static transient int total;
    protected boolean selected = false, factory = false, isbuilding = false, isunit = false, minable = false, fight = false,builder=false,isbase=false,isJedi=false,isTerrain=false;
    protected Gamemap game;
    protected int x, y, height, width, hitpoints, fullhitpoints, armor,buildspeed, clas, counter = 0,ID;
    //class type 1-foot, 2-armored, 3-building
    protected boolean visable = true;
    protected transient Image im;
    protected transient Image oldim;
    protected String name;
    protected Player owner;
    protected Resource cost;
    
    public GameObject(){
               ID=total;
        total++;
    }
    
    public GameObject(Gamemap newgame, Zone zon, int newhitpoints, String newname, Player newowner, int newclas, Resource newcost, int newbuildspeed) {
        game = newgame;
        x = zon.x;
        y = zon.y;
        height = zon.height;
        width = zon.width;
        owner = newowner;
        hitpoints = newhitpoints;
        fullhitpoints = newhitpoints;
        armor = 0; // temp
        owner = newowner;
        cost = newcost;
        buildspeed = newbuildspeed;
        name = newname;
        clas = newclas;
        ID=total;
        total++;
        oldim = (Image) game.getImages().get(name);
        setImageback();
    }
    
    
    public void load(String [] loadstring,GameDisplay dis)throws NumberFormatException{
        
        name=loadstring[0];
        ID=Integer.parseInt(loadstring[1]);

        x=Integer.parseInt(loadstring[3]);
        y=Integer.parseInt(loadstring[4]);
        hitpoints=Integer.parseInt(loadstring[5]);
        oldim = (Image) dis.Images.get(name);
        setImageback();
        
    }
    
    public String[] save(){
        String[]savestring=new String[20];
        //line 0 = name
        savestring[0]=name;
        //line 1= ID
        savestring[1]=Integer.toString(ID);
        //line 2 = owner (0,1,2)
        savestring[2]=Integer.toString(owner.getnum());
        //line 3 = x
        savestring[3]=Integer.toString(x);
        //line 4 = y
        savestring[4]=Integer.toString(y);
        //line 5 = hitpoints
        savestring[5]=Integer.toString(hitpoints);
        return savestring;
    }
    public int getID(){
        return ID;
    }
    public void setgame(Gamemap newgame){
        game=newgame;
        oldim = (Image) game.getImages().get(name);
        setImageback();
        for(Player tp:game.players)
        {
        	if(tp.equals(owner))
        	owner=tp;
        }
    }
    
    public void setlocation(int newx, int newy) {
        x=newx;
        y=newy;
    }
    
    public void setcost(Resource newcost){
        cost=newcost;
    }
    
    public boolean getUnit() {
        return isunit;
    }
    
    
    public boolean isBuilding() {
        return isbuilding;
    }
    
    
    public boolean isMinable() {
        return minable;
    }
    public boolean isBase(){
        return isbase;
    }
    
    
    public void tempsetImage(Image newimage) {
        im = newimage;
    }
    
    
    public void setImageback() {
        im = oldim;
    }
    
    public int getbuildspeed(){
        return buildspeed;
    }
    
    
    public boolean getfight() {
        return fight;
    }
    
    public boolean getJedi(){
        return isJedi;
    }
    
    public boolean getFactory() {
        return factory;
    }
    
    public String getName() {
        return name;
    }
    
    
    public int getfullhitpoints() {
        return fullhitpoints;
    }
    
    
    public int gethitpoints() {
        return hitpoints;
    }
    
    
    public void add() {
        game.add(this);
    }
    
    
    public GameObject createclone() {
        GameObject no;
        FightingUnit nf;
        ProductionBuilding npb;
        ProductionUnit npu;
        try {
            no = (GameObject) this.clone();
            if (fight) {
                nf = (FightingUnit) no;
                nf.setweapon(nf.getweapon().createclone());
                return nf;
            }
            if(factory && !isunit){
                ArrayList newproductionitems=new ArrayList();
                npb=(ProductionBuilding)no;
                for(int d=0;d<npb.productionitems.size();d++){
                    newproductionitems.add(((GameObject)npb.productionitems.get(d)).clone());
                }
                npb.productionitems=newproductionitems;
                npb.que = new LinkedList();
            }
            else if(isunit){
                ArrayList newproductionitems=new ArrayList();
                npu=(ProductionUnit)no;
                for(int d=0;d<npu.productionitems.size();d++){
                    newproductionitems.add(((GameObject)npu.productionitems.get(d)).clone());
                }
                npu.productionitems=newproductionitems;
                npu.que = new LinkedList();
            }
            return no;
        }
        catch (CloneNotSupportedException e) {
            new ErrorGen(game.dis,"CloneError",e);
            return null;
        }
    }
    
    public boolean getBuilder() {
        return builder;
    }
    
    public void add(Zone zon) {
        x = zon.x;
        y = zon.y;
        height = zon.height;
        width = zon.width;
        game.add(this);
    }
    
    
    public void die() {
        game.remove(this);
    }
    
    
    public boolean isVisable() {
        return visable;
    }
    
    
    public Point location() {
        return new Point(x, y);
    }
    
    
    public Resource getcost() {
        return cost;
    }
    
    
    //damagetype(StarCraft) -> 1=conncusive, 2=medium,3=explosive,4=normal,5=siege, 6=sniper
    //clas type 1-foot, 2- armored, 3-building
    public void dealdamage(int attackdamage, int damagetype) {
        //System.out.println ("Starting GameObject.dealdamage() attackdamage = " + attackdamage + " damagetype = " + damagetype); //Debug
        int realdamage = 0;
        switch (damagetype) {
            case 1://conncusive
                switch (clas) {
                    case 1:
                        realdamage += attackdamage;
                        break;
                    case 2:
                        realdamage += attackdamage / 5;
                        break;
                    case 3:
                        realdamage += attackdamage / 5;
                        break;
                    default:
                } //switch
                break;
            case 2://medium
                switch (clas) {
                    case 1:
                        realdamage += attackdamage / 2;
                        break;
                    case 2:
                        realdamage += attackdamage;
                        break;
                    case 3:
                        realdamage += attackdamage / 2;
                        break;
                    default:
                } //switch
                break;
            case 3://explosive
                switch (clas) {
                    case 1:
                        realdamage += attackdamage / 3.5;
                        break;
                    case 2:
                        realdamage += attackdamage;
                        break;
                    case 3:
                        realdamage += attackdamage / 2.5;
                        break;
                    default:
                } //switch
                break;
            case 4://normal
                realdamage += attackdamage;
                break;
            case 5://siege
                switch (clas) {
                    case 1:
                        realdamage += attackdamage / 4;
                        break;
                    case 2:
                        realdamage += attackdamage / 5;
                        break;
                    case 3:
                        realdamage += attackdamage;
                        break;
                    default:
                } //switch
                break;
            case 6://sniper
                switch (clas) {
                    case 1:
                        realdamage += attackdamage;
                        break;
                    case 2:
                        realdamage += attackdamage / 15;
                        break;
                    case 3:
                        realdamage += attackdamage/15;
                        break;
                    default:
                } //switch
            default:
        } //switch
        hitpoints -= realdamage;
        if (hitpoints <= 0 && exists()) {
            die();
            //  System.out.println ("Ending GameObject.dealdamage() Yes to death"); //Debug
        }
        //    System.out.println ("Ending GameObject.dealdamage() No to death"); //Debug
    }
    
    
    public boolean contains(Zone z) {
        if (z.x <= x && z.x + z.width >= x && z.y <= y && z.y + z.height >= y)  //new way must be squares
            return true;
        if (z.x <= x + width && z.x + z.width >= x + width && z.y <= y && z.y + z.height >= y)
            return true;
        if (z.x <= x && z.x + z.width >= x && z.y <= y + height && z.y + z.height >= y + height)
            return true;
        if (z.x <= x + width && z.x + z.width >= x + width && z.y <= y + height && z.y + z.height >= y + height)
            return true;
        //-- --
        if (x < z.x && x + width > z.x && y < z.y && y + height > z.y) //new way must be squares
            return true;
        if (x < z.x + z.width && x + width > z.x + z.width && y < z.y && y + height > z.y)
            return true;
        if (x < z.x && x + width > z.x && y < z.y + z.height && y + height > z.y + z.height)
            return true;
        if (x < z.x + z.width && x + width > z.x + z.width && y < z.y + z.height && y + height > z.y + z.height)
            return true;
        /*//old way***
        for (int w = 0 ; w < z.width ; w++)
        {
            for (int h = 0 ; h < z.height ; h++)
            {
                if (z.x + w > x && z.x + w < x + width && z.y + h > y && z.y + h < y + height)
                    return true;
            }
        }
         */
        return false;
    }
    
    public boolean contains(IZone z) {
        if (z.x <= x && z.x + z.width >= x && z.y <= y && z.y + z.height >= y)  //new way must be squares
            return true;
        if (z.x <= x + width && z.x + z.width >= x + width && z.y <= y && z.y + z.height >= y)
            return true;
        if (z.x <= x && z.x + z.width >= x && z.y <= y + height && z.y + z.height >= y + height)
            return true;
        if (z.x <= x + width && z.x + z.width >= x + width && z.y <= y + height && z.y + z.height >= y + height)
            return true;
        //-- --
        if (x < z.x && x + width > z.x && y < z.y && y + height > z.y) //new way must be squares
            return true;
        if (x < z.x + z.width && x + width > z.x + z.width && y < z.y && y + height > z.y)
            return true;
        if (x < z.x && x + width > z.x && y < z.y + z.height && y + height > z.y + z.height)
            return true;
        if (x < z.x + z.width && x + width > z.x + z.width && y < z.y + z.height && y + height > z.y + z.height)
            return true;
        /*//old way***
        for (int w = 0 ; w < z.width ; w++)
        {
            for (int h = 0 ; h < z.height ; h++)
            {
                if (z.x + w > x && z.x + w < x + width && z.y + h > y && z.y + h < y + height)
                    return true;
            }
        }
         */
        return false;
    }
    
    
    public boolean contains(Point p) {
        if (p.x > x && p.x < x + width && p.y > y && p.y < y + height)
            return true;
        return false;
    }
    
    
    public void select() {
        selected = true;
    }
    
    
    public void deselect() {
        selected = false;
    }
    
    
    public Player getowner() {
        return owner;
    }
    
    public void increasearmorlevel() {
        armor++;
    }
    
    
    public void increasehitpoints(int increase) {
        hitpoints += increase;
    }
    
    
    public boolean isAlly(GameObject newtarget) {
        return newtarget.getowner().getnum() == 0 || owner.getnum() == newtarget.getowner().getnum();
    }
    
    
    public void changeowner(Player newowner) {
        owner = newowner;
    }
    
    
    public void act()  //called by Gamemap.step()
    {
        counter++;
    }
    
    
    public IZone getImageZone() {
        return new IZone(im, x, y, width, height);
    }
    
    
    public Zone getZone() {
        return new Zone(x, y, width, height);
    }
    
    
    public Gamemap getgame() {
        return game;
    }
    
    public Point getCenter() {
        return new Point(x+width/2,y+height/2);
    }
    
    public void setTarget(GameObject no) {
    }
    
    
    public void setmoveTarget(Point no) {
    }
    
    
    public boolean exists() {
        return game.contains(this);
    }
}


