package JavaGameP;

public class FusionCutter extends HarvestingWeapon
{
    public FusionCutter (Harvester newholder)
    {
	super (newholder, 2, 8,  "FusionCutter");
	fireVolume=60;
    }
}
