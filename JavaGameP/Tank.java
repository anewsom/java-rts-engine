package JavaGameP;

public abstract class Tank extends FightingUnit
{
    public Tank(){
        super();
        clas=3;
    }
    public Tank (Gamemap newgame, Zone z, int newhitpoints, int newmovementrate, String newname, Player newowner, int newbuildspeed, Resource newcost,int newnum_attack_animation_frames, Cannon newweapon)
    {
	super (newgame, z, newhitpoints, newmovementrate, newname, newowner, 3, newbuildspeed, newcost,newnum_attack_animation_frames, newweapon);
    }
}
