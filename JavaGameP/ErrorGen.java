package JavaGameP;
import java.awt.*;
import javax.swing.JOptionPane;
import javax.sound.sampled.*;
import java.io.*;
/**
 *	ErrorGen --> is called whenever a game error occurs
 *	hides program, tries basic pause game attempt, displayed warning and then error details
 *	eventaully should be able to freeze all game activity and display error details in window
 */
public class ErrorGen{
  private String text;
    
    public ErrorGen(Frame gameDis,String CommonErrorName,Exception error) {
        if(gameDis!=null)
        {
        	GameDisplay.Display.forcepause=true;
        	gameDis.setState(Frame.ICONIFIED);
        }
        if(gameDis==null&&GameDisplay.onerun){
            GameDisplay.Display.forcepause=true;
            GameDisplay.Display.setVisible(false);
            GameDisplay.Display.stopmusic();
        }
        new File("running.tmp").delete();
        if(CommonErrorName.equals("CloneError"))
            text="CloneNotSupportedException has been thrown";
        else if(CommonErrorName.equals("SoundError"))
            text="Sound Error has been thrown";
        else if(CommonErrorName.equals("ThreadError"))
            text="InterruptedException has been thrown";
            else if(CommonErrorName.equals("NFE"))
            text="Number Format Error has been thrown";
            else if(CommonErrorName.equals("IOE"))
            text="IOException has been thrown";
        else
            text=CommonErrorName;

        try {
            File tempfile = new File("Error Message.wav");
            AudioInputStream audioSource = AudioSystem.getAudioInputStream(tempfile);
            DataLine.Info info = new DataLine.Info(Clip.class, audioSource.getFormat());
            Clip backgroundclip = (Clip)AudioSystem.getLine(info);
            backgroundclip.open(audioSource);
            backgroundclip.start();
            }
        catch (Exception e) {}
        if(gameDis!=null)
        	((GameDisplay)gameDis).debugger.close();
        try{Thread.sleep(1000);
        }catch(InterruptedException e){}
        JOptionPane.showMessageDialog(new Frame(),text + "  || Printing stack trace: ","Game Error",JOptionPane.ERROR_MESSAGE); 
        error.printStackTrace();       
    }
}
